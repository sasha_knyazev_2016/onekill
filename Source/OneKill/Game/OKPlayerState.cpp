// Fill out your copyright notice in the Description page of Project Settings.


#include "OKPlayerState.h"
#include "Net/UnrealNetwork.h"



void AOKPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOKPlayerState, MyKill);
	DOREPLIFETIME(AOKPlayerState, MyDeath);
	DOREPLIFETIME(AOKPlayerState, IsDeath);
	DOREPLIFETIME(AOKPlayerState, MyName);
	DOREPLIFETIME(AOKPlayerState, MyTeam);	
}


void AOKPlayerState::BeginPlay()
{
	Super::BeginPlay();
}

void AOKPlayerState::SetDeathToPlayerState(int NewDeath)
{
	MyDeath = NewDeath;
}

void AOKPlayerState::SetDeath_OnServer_Implementation()
{
	CurrentDeath = CurrentDeath + 1;
	SetDeathToPlayerState(CurrentDeath);
}

void AOKPlayerState::SetDeath_Multicast_Implementation()
{
	CurrentDeath = CurrentDeath + 1;
	SetDeathToPlayerState(CurrentDeath);
}



