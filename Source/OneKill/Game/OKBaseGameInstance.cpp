// Fill out your copyright notice in the Description page of Project Settings.


#include "OKBaseGameInstance.h"

bool UOKBaseGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UOKBaseGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return bIsFind;
}
bool UOKBaseGameInstance::GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow;
		TArray<FName>RowNames = DropItemInfoTable->GetRowNames();

		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.NameItem == NameItem)
			{
				OutInfo = (*DropItemInfoRow);
				bIsFind = true;
			}
			i++;//fix
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UOKBaseGameInstance::GetDropItemInfoByName - DropItemInfoTable -NULL"));
	}

	return bIsFind;
}
bool UOKBaseGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	
	bool bIsFind = false;
	FDropItem* DropItemInfoRow;
	if (DropItemInfoTable)
	{
		DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(NameItem, "", false);
		if (DropItemInfoRow)
		{
			bIsFind = true;
			OutInfo = *DropItemInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UOKBaseGameInstance::GetDropItemInfoByName - WeaponTable -NULL"));
	}
	
	return bIsFind;
}

void UOKBaseGameInstance::SaveCurrentNamePlayers(FName NamePlayer, int CurrentPlayerID)
{
	CurrentNamePlayer.Add(NamePlayer);
	CurrentIdPlayer.Add(CurrentPlayerID);
}

void UOKBaseGameInstance::SaveCurrentNameWeapon(FName NameWeapon, int CurrentWeaponID)
{
	CurrentNameWeapon.Add(NameWeapon);
	CurrentIdWeapon.Add(CurrentWeaponID);
}
void UOKBaseGameInstance::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UOKBaseGameInstance, CurrentNameWeapon);
	DOREPLIFETIME(UOKBaseGameInstance, CurrentNamePlayer);
	DOREPLIFETIME(UOKBaseGameInstance, CurrentIdPlayer);
	DOREPLIFETIME(UOKBaseGameInstance, CurrentIdWeapon);
}