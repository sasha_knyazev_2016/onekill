// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "../Weapon/OKBaseWeapon.h"
#include "../Weapon/OKBaseProjectile.h"
#include "Engine/GameInstance.h"
#include "OKBaseGameInstance.generated.h"

/**
 * 
 */

UCLASS()
class ONEKILL_API UOKBaseGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
	UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
	UDataTable* DropItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


	UFUNCTION(BlueprintCallable)
	void SaveCurrentNamePlayers(FName NamePlayer, int CurrentPlayerID);

	UFUNCTION(BlueprintCallable)
	void SaveCurrentNameWeapon(FName NameWeapon, int CurrentWeaponID);


	UPROPERTY(Replicated, BlueprintReadWrite)
	TArray<FName> CurrentNameWeapon;
	UPROPERTY(Replicated, BlueprintReadWrite)
	TArray<FName> CurrentNamePlayer;
	UPROPERTY(Replicated, BlueprintReadWrite)
	TArray<int> CurrentIdPlayer;
	UPROPERTY(Replicated, BlueprintReadWrite)
	TArray<int> CurrentIdWeapon;

};
