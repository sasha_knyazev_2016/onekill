// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "GameFramework/PlayerState.h"
#include "OKPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class ONEKILL_API AOKPlayerState : public APlayerState
{
	GENERATED_BODY()
public:


	void SetDeathToPlayerState(int NewDeath);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetDeath_Multicast();
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetDeath_OnServer();

	int CurrentDeath = 0;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character | Info")
	int MyKill = 0;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character | Info")
	int MyDeath = 0;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character | Info")
	bool IsDeath;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character | Info")
	FName MyName;
	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Character | Info")
	int MyTeam = 0;

	

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	/*
	* UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetMyNameFromLobby_Multicast();
	*/
	

	virtual void BeginPlay() override;
};
