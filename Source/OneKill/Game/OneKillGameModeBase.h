// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameModeBase.h"
#include "OneKillGameModeBase.generated.h"

class AOKBasePlayerController;
class AOKBaseCharacter;
/**
 * 
 */
UCLASS()
class ONEKILL_API AOneKillGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	

	virtual void PostLogin(APlayerController* NewPlayer) override;
public:

	
	UFUNCTION(BlueprintNativeEvent)
	void PostLoginBP(APlayerController* NewPlayer);
	UFUNCTION(BlueprintPure)
	int GetScoreToCommand(int& ScoreTimeTwo);

	UFUNCTION(BlueprintCallable)
	void EndGameWin();
	UFUNCTION(BlueprintCallable)
	void SetScoreOneCommand();
	UFUNCTION(BlueprintCallable)
	void SetScoreTwoCommand();
	UFUNCTION(BlueprintCallable)
	void SetPlayersToCommandRed(AOKBaseCharacter* Players);
	UFUNCTION(BlueprintCallable)
	void SetPlayersToCommandBlue(AOKBaseCharacter* Players);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetScoreToComand(int Id);


	UPROPERTY(Replicated, EditAnywhere, Category = "NetInformation")
	TSoftObjectPtr<AOKBasePlayerController> CashBasePlayerController;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	AOKBasePlayerController* NewController;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	TArray<APlayerController*> CurrentPlayers;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	TArray<AOKBaseCharacter*> PlayersBlueCollors;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	TArray<AOKBaseCharacter*> PlayersRedCollors;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	int CurrentIndexLastPlayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NetInformation")
	int ScoreOneCommand;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NetInformation")
	int ScoreTwoCommand;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NetInformation")
	int MaxScore;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	FName TagsPlayer;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	FName NameTeamOne;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	FName NameTeamTwo;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	UObject* IconTeamOne;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	UObject* IconTeamTwo;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	bool bIsEndGame;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	TArray<AActor*> PlayerControllers;
	
	
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	TArray<AOKBasePlayerController*> AllControllers;
	UPROPERTY(Replicated, EditAnywhere, Category = "NetInformation")
	TSoftObjectPtr<AOKBasePlayerController> CashBaseController;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	TArray<AActor*> AllPlayers;
	

	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


};


