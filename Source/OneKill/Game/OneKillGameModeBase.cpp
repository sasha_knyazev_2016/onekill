// Copyright Epic Games, Inc. All Rights Reserved.


#include "OneKillGameModeBase.h"
#include "Net/UnrealNetwork.h"
#include "../Controller/OKBasePlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

void AOneKillGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	PostLoginBP(NewPlayer);
	CurrentPlayers.Add(NewPlayer);
	CurrentIndexLastPlayer = CurrentPlayers.Num();
	CashBasePlayerController = Cast<AOKBasePlayerController>(NewPlayer);
	NewController = CashBasePlayerController.Get();
	NewController->StartPlay_Multicast();
}

int AOneKillGameModeBase::GetScoreToCommand(int& ScoreTimeTwo)
{
	ScoreTimeTwo = ScoreTwoCommand;
	return ScoreOneCommand;
}

void AOneKillGameModeBase::EndGameWin()
{
	if (ScoreOneCommand == MaxScore)
	{
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), TagsPlayer, AllPlayers);
		for (size_t i = 0; i < AllPlayers.Num(); i++)
		{
			NewController->EndGameWin_OnServer(ScoreOneCommand, ScoreTwoCommand, NameTeamOne, IconTeamOne);
			bIsEndGame = true;
			
			//UWidgetBlueprintLibrary::SetInputMode_UIOnlyEx(NewController, nullptr, EMouseLockMode::DoNotLock, true);
			NewController->bShowMouseCursor = true;
		}
	}
	else if (ScoreTwoCommand == MaxScore)
	{
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), TagsPlayer, AllPlayers);
		for (size_t i = 0; i < AllPlayers.Num(); i++)
		{
			NewController->EndGameWin_OnServer(ScoreOneCommand, ScoreTwoCommand, NameTeamTwo, IconTeamTwo);
			bIsEndGame = true;
			
			//UWidgetBlueprintLibrary::SetInputMode_UIOnlyEx(NewController, nullptr, EMouseLockMode::DoNotLock, true);
			NewController->bShowMouseCursor = true;
		}
	}
	
}

void AOneKillGameModeBase::SetScoreOneCommand()
{
	ScoreOneCommand = ScoreOneCommand + 1;
}

void AOneKillGameModeBase::SetScoreTwoCommand()
{
	ScoreTwoCommand = ScoreTwoCommand + 1;
}

void AOneKillGameModeBase::SetPlayersToCommandRed(AOKBaseCharacter* Players)
{
	PlayersRedCollors.Add(Players);
}

void AOneKillGameModeBase::SetPlayersToCommandBlue(AOKBaseCharacter* Players)
{
	PlayersBlueCollors.Add(Players);
}

void AOneKillGameModeBase::SetScoreToComand_Implementation(int Id)
{
	
	if (Id % 2 == 0)
	{
		ScoreOneCommand = ScoreOneCommand + 1;
		EndGameWin();
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Controller", PlayerControllers);
		for (int i = 0; PlayerControllers.Last(); i++)
		{
			//CashBaseController = Cast<AOKBasePlayerController>(PlayerControllers[i]);
			//CashBaseController.Get()->SetScoreToCommand(ScoreOneCommand, ScoreTwoCommand, Id);
		}
	}
	else
	{
		ScoreTwoCommand = ScoreTwoCommand + 1;
		EndGameWin();
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Controller", PlayerControllers);
		for (int j = 0; PlayerControllers.Last(); j++)
		{
			//CashBaseController = Cast<AOKBasePlayerController>(PlayerControllers[j]);
			//CashBaseController.Get()->SetScoreToCommand(ScoreTwoCommand, ScoreOneCommand, Id);
		}

	}
}

void AOneKillGameModeBase::PostLoginBP_Implementation(APlayerController* NewPlayer)
{
}


void AOneKillGameModeBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOneKillGameModeBase, CurrentIndexLastPlayer);
	DOREPLIFETIME(AOneKillGameModeBase, PlayersRedCollors);
	DOREPLIFETIME(AOneKillGameModeBase, PlayersBlueCollors);

	
	//DOREPLIFETIME(AOneKillGameModeBase, IsEndGame);
	
	
}