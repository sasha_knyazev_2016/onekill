// Copyright Epic Games, Inc. All Rights Reserved.

#include "OneKill.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, OneKill, "OneKill" );
