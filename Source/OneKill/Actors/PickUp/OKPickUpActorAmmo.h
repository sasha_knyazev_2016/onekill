// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OKPickUpActorBase.h"
#include "OKPickUpActorAmmo.generated.h"

/**
 * 
 */
UCLASS()
class ONEKILL_API AOKPickUpActorAmmo : public AOKPickUpActorBase
{
	GENERATED_BODY()
	
protected:

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	EWeaponType WeaponTypeAmmo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	int Cout;
	
};
