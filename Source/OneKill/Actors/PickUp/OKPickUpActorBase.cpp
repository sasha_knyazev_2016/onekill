// Fill out your copyright notice in the Description page of Project Settings.


#include "OKPickUpActorBase.h"

// Sets default values
AOKPickUpActorBase::AOKPickUpActorBase()
{
	SpherComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}



void AOKPickUpActorBase::BeginPlay()
{
	Super::BeginPlay();
	SpherComponent->OnComponentBeginOverlap.AddDynamic(this, &AOKPickUpActorBase::CharCollisionSpherBeginOverlap);
}

void AOKPickUpActorBase::PickUpSuccess()
{
	Destroy();
}


 void AOKPickUpActorBase::CharCollisionSpherBeginOverlap(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	CachedBaseCharacter = Cast<AOKBaseCharacter>(OtherActor);
	Inventory = CachedBaseCharacter->InventoryComponent;
}



