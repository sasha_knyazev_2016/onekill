// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"


#include"../../Component/OKInventoryComponent.h"
#include "../../Character/OKBaseCharacter.h"
#include "GameFramework/Actor.h"
#include "OKPickUpActorBase.generated.h"

UCLASS()
class ONEKILL_API AOKPickUpActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOKPickUpActorBase();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	class USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	USphereComponent* SpherComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	UStaticMeshComponent* MeshComponent;
	UFUNCTION(BlueprintCallable)
	void PickUpSuccess();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UOKInventoryComponent* Inventory;
	UFUNCTION()
	void CharCollisionSpherBeginOverlap(class UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:
	TSoftObjectPtr<AOKBaseCharacter> CachedBaseCharacter;
	
};
