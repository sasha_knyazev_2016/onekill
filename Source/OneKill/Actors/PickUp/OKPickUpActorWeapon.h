// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OKPickUpActorBase.h"
#include "OKPickUpActorWeapon.generated.h"

/**
 * 
 */
UCLASS()
class ONEKILL_API AOKPickUpActorWeapon : public AOKPickUpActorBase
{
	GENERATED_BODY()
	
};
