// Fill out your copyright notice in the Description page of Project Settings.


#include "OKCharacterAnimInstance.h"

void UOKCharacterAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
	checkf(TryGetPawnOwner()-> IsA<AOKBaseCharacter>(), TEXT("UOKCharacterAnimInstance::NativeBeginPlay()"));
	CachedBaseCharacter = StaticCast<AOKBaseCharacter*>(TryGetPawnOwner());
}

void UOKCharacterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if (!CachedBaseCharacter.IsValid())
	{
		return;
	}
	UCharacterMovementComponent* CharacterMovement = CachedBaseCharacter->GetCharacterMovement();

	Speed = CharacterMovement->Velocity.Size();
	bIsFalling = CharacterMovement->IsFalling();
	bIsAiming = CachedBaseCharacter->GetIsAiming();
	bIsCrouching = CharacterMovement->IsCrouching();

	bIsStraifing = !CharacterMovement->bOrientRotationToMovement;
	Direction = CalculateDirection(CharacterMovement->Velocity, CachedBaseCharacter->GetActorRotation());
	
	AimRotation = CachedBaseCharacter->CustomRotationAnim;
	
}
void UOKCharacterAnimInstance::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UOKCharacterAnimInstance, Direction);
	DOREPLIFETIME(UOKCharacterAnimInstance, AimRotation);
	
}
