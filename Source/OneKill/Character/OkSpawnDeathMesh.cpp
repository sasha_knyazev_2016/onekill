// Fill out your copyright notice in the Description page of Project Settings.


#include "OkSpawnDeathMesh.h"
#include "Materials/MaterialInterface.h"
// Sets default values
AOkSpawnDeathMesh::AOkSpawnDeathMesh()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;
	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
	SkeletalMeshComponent->SetGenerateOverlapEvents(false);
	SkeletalMeshComponent->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshComponent->SetupAttachment(RootComponent);
	
}

void AOkSpawnDeathMesh::DestroyTimer()
{
	Destroy();
}

void AOkSpawnDeathMesh::SetMaterial_Multicast_Implementation(UMaterialInterface* Collor)
{
	SkeletalMeshComponent->SetMaterial(0, Collor);
}

// Called when the game starts or when spawned
void AOkSpawnDeathMesh::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(TimerHandleDestroyTimer, this, &AOkSpawnDeathMesh::DestroyTimer, DeathMontage->GetPlayLength(), false);
}



