// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OKBaseCharacter.h"
#include "GameFramework/SpectatorPawn.h"
#include "OKSpectatorPawn.generated.h"

/**
 * 
 */
UCLASS()
class ONEKILL_API AOKSpectatorPawn : public ASpectatorPawn
{
	GENERATED_BODY()
public:
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;
	
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetActorLocation_Multicast(AOKBaseCharacter* CharStalker);
	UFUNCTION(BlueprintNativeEvent)
	void SetActorLocation_BP_Multicast();
	UFUNCTION(BlueprintCallable)
	void SetMyTransform(AOKBaseCharacter* CharStalker);
	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void SetInfoBySpectator_Multicast(AOKBasePlayerController* SpectatorController, AOKBaseCharacter* CharStalker);
	UFUNCTION(BlueprintCallable)
	TArray<AOKBaseCharacter*> GetCurrentPlayersStalker(TArray<AOKBaseCharacter*> AllPlayers, bool& IsAllDead);
	UFUNCTION(BlueprintCallable, Server, Reliable)
	void GetCurrentPlayers_OnServer(const TArray<AOKBaseCharacter*>& AllPlayers);

	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void SpectatePriviousPlayer_Multicast(const TArray<AOKBaseCharacter*>& Players, AOKBasePlayerController* ControllerSpaectator);
	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void SpectateNextPlayer_Multicast(const TArray<AOKBaseCharacter*>& Players, AOKBasePlayerController* ControllerSpaectator);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SpectatePriviousPlayer_BP(const TArray<AOKBaseCharacter*>& Players, AOKBasePlayerController* ControllerSpaectator);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SpectateNextPlayer_BP(const TArray<AOKBaseCharacter*>& Players, AOKBasePlayerController* ControllerSpaectator);
	UFUNCTION(BlueprintCallable)
	void GetCurrentPriviosPlayerToSpectate();
	UFUNCTION(BlueprintCallable)
	void GetCurrentNextPlayerToSpectate();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void PVSpectateNextPlayer_BP(AOKBaseCharacter* PlayerSpaectate);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void PVSpectatePriviosPlayer_BP(AOKBaseCharacter* PlayerSpaectate);

	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void NXSpectateNextPlayer_BP(AOKBaseCharacter* PlayerSpaectate);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void NXSpectatePriviosPlayer_BP(AOKBaseCharacter* PlayerSpaectate);

	
	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void SetMyTransform_Multicast(AOKBaseCharacter* CharStalker);

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	TArray<AOKBaseCharacter*> PlayerCollor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	bool bIsAllDead;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	AOKBaseCharacter* PlayerStalker;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AOKBasePlayerController* CurrentControllerSpectator;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrentSpectatorPlayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	AOKBaseCharacter* BaseCharStalker;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	TArray<AOKBaseCharacter*> PlayersCollor;

	
	virtual void GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const override;
};







