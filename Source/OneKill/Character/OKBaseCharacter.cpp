// Fill out your copyright notice in the Description page of Project Settings.
// Fill out your copyright notice in the Description page of Project Settings.


#include "OKBaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "../Game/OKPlayerState.h"
#include "../Game/OKBaseGameInstance.h"
#include "Components/CapsuleComponent.h"
#include "Components/SplineComponent.h"
#include "Camera/CameraComponent.h"
#include "Materials/MaterialInterface.h"
#include "Net/UnrealNetwork.h"
#include "../Controller/OKBasePlayerController.h"



AOKBaseCharacter::AOKBaseCharacter()
{
	SetReplicates(true);
	InventoryComponent = CreateDefaultSubobject<UOKInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AOKBaseCharacter::InitWeapon);
	}
	HealthComponent = CreateDefaultSubobject<UOKHealthCharacterComponent>(TEXT("HealthComponent"));
	bReplicates = true;

	FirstPersonArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("FirstPersonArrowComponent"));
	FirstPersonArrowComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMeshComponent"));
	FirstPersonMeshComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonMeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, -128.0f));
	FirstPersonMeshComponent->CastShadow = false;
	FirstPersonMeshComponent->bCastDynamicShadow = false;
	FirstPersonMeshComponent->SetOnlyOwnerSee(true);

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(FirstPersonMeshComponent, SoketFPCamera);
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	MeshWeaponComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshWeaponComponent"));
	MeshWeaponComponent->SetupAttachment(GetMesh());
	
	//MeshWeaponComponent->SetupAttachment(GetMesh());
	FirstPersonMeshWeaponComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMeshWeaponComponent"));
	FirstPersonMeshWeaponComponent->SetupAttachment(FirstPersonMeshComponent);
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	GetMesh()->SetOwnerNoSee(true);
	GetMesh()->bCastHiddenShadow = true;
	
	MeshWeaponComponent->SetOwnerNoSee(true);
	MeshWeaponComponent->bCastHiddenShadow = true;
	//CameraComponent->bAutoActivate = false;
	//SpringArmComponent->bAutoActivate = false;
	//SpringArmComponent->bUsePawnControlRotation = false;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	bUseControllerRotationYaw = true;
}



void AOKBaseCharacter::Aiming()
{
	if (CurrentWeapon)
		SetAiming_OnServer(true);
	//bIsAiming = true;
	CurrentWeapon->UpdateDispertionWeapon(GetCurrentWeapon()->WeaponSetting.DispertionWeapon.Aim_StateDispersionAimMax, GetCurrentWeapon()->WeaponSetting.DispertionWeapon.Aim_StateDispersionAimMin, GetCurrentWeapon()->WeaponSetting.DispertionWeapon.Aim_StateDispersionAimRecoil, GetCurrentWeapon()->WeaponSetting.DispertionWeapon.Aim_StateDispersionReduction);
}

void AOKBaseCharacter::DeaAiming()
{
	if (CurrentWeapon)
		SetAiming_OnServer(false);
	//bIsAiming = false;
	CurrentWeapon->UpdateDispertionWeapon(GetCurrentWeapon()->WeaponSetting.DispertionWeapon.Walk_StateDispersionAimMax, GetCurrentWeapon()->WeaponSetting.DispertionWeapon.Walk_StateDispersionAimMin, GetCurrentWeapon()->WeaponSetting.DispertionWeapon.Walk_StateDispersionAimRecoil, GetCurrentWeapon()->WeaponSetting.DispertionWeapon.Aim_StateDispersionReduction);
	

	GetCharacterMovement()->MaxWalkSpeed = DefaultMaxMovementSpeed;

}

void AOKBaseCharacter::SetAiming_OnServer_Implementation(bool Aim)
{
	bIsAiming = Aim;
}

void AOKBaseCharacter::ChangeCrouchState()
{
	if (GetCharacterMovement()->IsCrouching())
	{
		UnCrouch();
	}
	else
	{
		Crouch();
	}
}

void AOKBaseCharacter::StartSprint()
{
	SetSprintRequested_OnServer(true);
	
}

void AOKBaseCharacter::StopSprint()
{
	SetSprintRequested_OnServer(false);
}

void AOKBaseCharacter::StartAttack()
{
	if (!bIsSprintRequested)
	{
		AttackCharEvent(true);
	}
}

void AOKBaseCharacter::StopAttack()
{
	AttackCharEvent(false);
}

void AOKBaseCharacter::AttackCharEvent(bool bIsFiring)
{
	AOKBaseWeapon* myWeapom = nullptr;
	myWeapom = CurrentWeapon;
	if (myWeapom)
	{
		CurrentWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	//else
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("AOKBaseCharacter::AttackCharEvent - CurrentWeapon = null"))
	//}
}

void AOKBaseCharacter::TryReload()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponRealoading)
	{
		
		TryReload_OnServer();	
	}
}

void AOKBaseCharacter::SwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num()>1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponRealoading)
			{
				CurrentWeapon->CancelReload();
			}
			if (InventoryComponent)
			{
				if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
				{
				}
			}
		}
	}
}

void AOKBaseCharacter::SwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponRealoading)
			{
				CurrentWeapon->CancelReload();
			}
			if (InventoryComponent)
			{
				if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
				{
				}
			}
		}
	}
}

void AOKBaseCharacter::ActionWeaponSlot1()
{
	ActionWeaponSlot1_BP();
}

void AOKBaseCharacter::ActionWeaponSlot2()
{
	ActionWeaponSlot2_BP();
}

void AOKBaseCharacter::Shoot()
{

	FVector Start = GetActorLocation();
	FVector End = GetActorLocation() + GetActorForwardVector() * 200;
	FHitResult HitResult;
	FCollisionQueryParams  COQP;
	COQP.AddIgnoredActor(this);
	FCollisionResponseParams CollRes;
	if (!GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_Visibility, COQP, CollRes))
	{
		Shoot_BP();
	}
	
	
	

	
}

void AOKBaseCharacter::ScoreBoard()
{
	if (!bISActiveScoreBoard)
	{
		bISActiveScoreBoard = true;
		ScoreBoardActive_BP();
	}
	else
	{
		bISActiveScoreBoard = false;
		ScoreBoardDeactive_BP();
	}
}

void AOKBaseCharacter::ScoreBoardDeactive_BP_Implementation()
{
}

void AOKBaseCharacter::ScoreBoardActive_BP_Implementation()
{
}

void AOKBaseCharacter::Shoot_BP_Implementation()
{
}

void AOKBaseCharacter::ActionWeaponSlot1_BP_Implementation()
{
}

void AOKBaseCharacter::ActionWeaponSlot2_BP_Implementation()
{
}



void AOKBaseCharacter::InitWeapon(FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalWeaponInfo, int32 NewCurrentIndexWeapon)
{
	//PlayAnimMontage(SwitchWeaponhMontage);
	
	if (CurrentWeapon)
	{
			CurrentWeapon->Destroy();
			CurrentWeapon = nullptr;
	}
	UOKBaseGameInstance* myGI = Cast<UOKBaseGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(WeaponIdName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AOKBaseWeapon* myWeapon = Cast<AOKBaseWeapon>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
					{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSoketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					CurrentIndexWeapon = NewCurrentIndexWeapon;
					//if (InventoryComponent)
					//{
						//CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(WeaponIdName);
					//}

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AOKBaseCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AOKBaseCharacter::WeaponReloadEnd);						
					myWeapon->FOnWeaponFire.AddDynamic(this, &AOKBaseCharacter::WeaponFireAnim);
					
					myWeapon->myOwner = this;
					CurrentweaponInfo = myWeaponInfo;
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					{
						CurrentWeapon->IniteReload();
					}
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
					}
			}
		}
	}
	
}

FWeaponInfo AOKBaseCharacter::GetWeaponImfo()
{
	return CurrentweaponInfo;
}

AOKBaseWeapon* AOKBaseCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

FVector AOKBaseCharacter::SetCharacterToCommandLocation(int IndexCommand)
{
	
	FVector Mylocation;
	for (int i =0; i< FoundEnemies.Num(); i++)
	{
		Mylocation = FoundEnemies[i]->GetTransform().GetLocation();
		if (IndexCommand == i)
		{

			Mylocation = FVector(Mylocation.X + FMath::FRandRange(-200, 500), Mylocation.Y, Mylocation.Z);
			SetActorLocation(Mylocation,false);
			return Mylocation;
			break;
		}
	}
	return Mylocation;
}

FVector AOKBaseCharacter::SetCharacterToCommandLocationToId(int IndexCommand)
{
	FVector MyVectorTeam;
	if (IndexCommand % 2 == 0)
	{
		MyCurrentTeam = 0;
		MyVectorTeam = SetCharacterToCommandLocation(MyCurrentTeam);
		bIsDead = false;
		SetCollor_Multicast(CollorTeamOne);
	}
	else
	{
		MyCurrentTeam = 1;
		MyVectorTeam = SetCharacterToCommandLocation(MyCurrentTeam);
		bIsDead = false;
		SetCollor_Multicast(CollorTeamTwo);
	}
	return MyVectorTeam;
}

void AOKBaseCharacter::SetCollor_OnServer_Implementation()
{
	SetTrueCollor(GetPlayerState()->GetPlayerId());
	GetMesh()->SetMaterial(0, CurrentCollor);
	SetCollor_Multicast(CurrentCollor);
}

void AOKBaseCharacter::SetCollor_Multicast_Implementation(UMaterialInterface* NewMaterial)
{
	GetMesh()->SetMaterial(0, NewMaterial);
	CurrentCollor = NewMaterial;
}

void AOKBaseCharacter::SetTrueCollor(int Id)
{
	
	if (Id == GetPlayerState()->GetPlayerId())
	{
		if (GetPlayerState()->GetPlayerId() % 2 == 0)
		{
			CurrentCollor = CollorTeamOne;
		}
		else
		{
			CurrentCollor = CollorTeamTwo;
		}
	}
	
}

void AOKBaseCharacter::SetKill_Multicast_Implementation()
{
	CurrentMyKills = CurrentMyKills + 1;
	CachedBasePlayerState = Cast<AOKPlayerState>(GetPlayerState());
	CachedBasePlayerState.Get()->MyKill = CurrentMyKills;
}

void AOKBaseCharacter::SetScoreBP_Implementation()
{

}

void AOKBaseCharacter::SetScore_Multicast_Implementation()
{
	SetScoreBP();
}

void AOKBaseCharacter::PlayHitMontage_Multicast_Implementation(AOKBaseCharacter* DamegeChar)
{
	if (DamegeChar->HealthComponent->GetCurreHealth() > 40)
	{
		PlayAnimMontage(AnimationHit);
	}
}


void AOKBaseCharacter::SetCurrentWeaponForLobby_Multicast_Implementation()
{
	CachedBaseGameInstance = Cast<UOKBaseGameInstance>(GetGameInstance());
	CachedBasePlayerState = Cast<AOKPlayerState>(GetPlayerState());
	int CurrentId;
	for (int i = 0; i < CachedBaseGameInstance.Get()->CurrentIdWeapon.Num(); i++)
	{
		if (CachedBasePlayerState.Get()->GetPlayerId() == CachedBaseGameInstance.Get()->CurrentIdWeapon[i])
		{
			CurrentId = i;
				break;
		}
	}
	for (int j = 0; j < CachedBaseGameInstance.Get()->CurrentNameWeapon.Num(); j++)
	{
		if (CurrentId == j)
		{
			CurrentNameWeapon = CachedBaseGameInstance.Get()->CurrentNameWeapon[j];
			break;
		}
	}
}


void AOKBaseCharacter::GetPlayerInfoToNetworkBeginPlay_Multicast_Implementation(APlayerState* CurrentState)
{
	//GetPlayerState
	//CachedBasePlayerState = Cast<AOKPlayerState>(GetPlayerState());
	//CurrentPlayerState = CachedBasePlayerState.Get();
	SetCharacterToCommandLocationToId(CurrentState->GetPlayerId());
	//CachedBaseGameMode = Cast<AOneKillGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	SetCurrentNameFromLobby_Multicast();
	//CurrentIdPlayer = CachedBasePlayerState.Get()->GetPlayerId();
	//CreateWeapon();
	
		
	
	
}
void AOKBaseCharacter::SetCurrentNameFromLobby_Multicast_Implementation()
{
	CachedBaseGameInstance = Cast<UOKBaseGameInstance>(GetGameInstance());
	CachedBasePlayerState = Cast<AOKPlayerState>(GetPlayerState());
	CachedBasePlayerState.Get()->GetPlayerId();
	int CurrentId;
	for (int i = 0; i < CachedBaseGameInstance.Get()->CurrentIdPlayer.Num(); i++)
	{
		if (GetPlayerState()->GetPlayerId() == CachedBaseGameInstance.Get()->CurrentIdPlayer[i])
		{
			CurrentId = i;
			break;
		}
	}
	for (int j = 0; j < CachedBaseGameInstance.Get()->CurrentNamePlayer.Num(); j++)
	{
		if (CurrentId == j)
		{
			CurrentName = CachedBaseGameInstance.Get()->CurrentNamePlayer[j];
			CachedBasePlayerState.Get()->MyName = CurrentName;
			break;
		}
	}
}

void AOKBaseCharacter::TryReload_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
	{
		CurrentWeapon->IniteReload();

	}
}

void AOKBaseCharacter::TryReloadWidget_BP_Implementation()
{

}

void AOKBaseCharacter::WeaponReloadStart(UAnimMontage* AnimReload, UAnimMontage* FPSAnimReload,  UAnimMontage* FPSAnimReloadWeapon, UAnimMontage* AnimReloadWeapon)
{
	bIsReloading = true;
	ReloadAnim_Multicast(AnimReload, FPSAnimReload, FPSAnimReloadWeapon, AnimReloadWeapon);
	//PlayAnimMontage(AnimReload);
}

void AOKBaseCharacter::WeaponReloadEnd(bool Success, int32 AmmoSafe)
{
	
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
		TryReloadWidget_BP();
	}
	bIsReloading = false;
}
void AOKBaseCharacter::WeaponFireAnim(UAnimMontage* AnimFireChar,UAnimMontage* AnimFireFPSChar, UAnimMontage* AnimFireWeaponFPS, UAnimMontage* AnimFireWeapon, UAnimMontage* AnimFireAnim)
{
	Shoot();
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon,CurrentWeapon->AdditionalWeaponInfo);
	}
	if (GetIsAiming())
	{
		AnimFireAnim_Multicast(AnimFireAnim,AnimFireFPSChar, AnimFireWeaponFPS, AnimFireWeapon);
	}
	else
	{
		FireAnim_Multicast(AnimFireChar, AnimFireFPSChar, AnimFireWeaponFPS, AnimFireWeapon);
	}
	
}

float AOKBaseCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	HealthComponent->IncominDamage(-DamageAmount);
	return ActualDamage;
}

void AOKBaseCharacter::CharDead()
{
	CharRespawn();
}

void AOKBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DefaultMaxMovementSpeed = 600.0f;
	if (bIsSprintRequested && !bIsSprinting && CanSprint())
	{
		SetIsSprinting_OnServer(true);
		SetMaxWallkSpeed_Multicast(SprintSpeed);
	}
	if (!bIsSprintRequested && bIsSprinting)
	{
		SetIsSprinting_OnServer(false);
		SetMaxWallkSpeed_Multicast(DefaultMaxMovementSpeed);
	}
	if (bIsAiming)
	{
		SetMaxWallkSpeed_Multicast(SpeedAiming);
	}
	if (!bIsAiming && !bIsSprinting)
	{
		SetMaxWallkSpeed_Multicast(DefaultMaxMovementSpeed);
	}

	CustomRotationAnim = FRotator(GetBaseAimRotation().Roll, GetBaseAimRotation().Pitch + 360.0f, GetBaseAimRotation().Yaw);
}



void AOKBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	//CurrentController = CachedBaseController.Get();
	//GetWorld()->GetTimerManager().SetTimer(BeginPlayMulticastTimer, this, &AOKBaseCharacter::GetPlayerInfoToNetworkBeginPlay_Multicast, 1.4f, false);
	//SetCurrentWeaponForLobby_Multicast();
	
	
	//SetLocationToCommand
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "Command", FoundEnemies);
	//GetPlayerInfoToNetworkBeginPlay_Multicast();
	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &AOKBaseCharacter::CharDead);
	}
	FAdditionalWeaponInfo WeaponAdditionalWeaponOnfo;
	
}

bool AOKBaseCharacter::GetIsAiming()
{
	return bIsAiming;
}

bool AOKBaseCharacter::GetIsLive()
{
	return bIsLive;
}

void AOKBaseCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
	CharRespawn();
}



void AOKBaseCharacter::CharRespawn_Multicast_Implementation()
{
	if (GetPlayerState()->GetPlayerId() % 2 == 0)
	{
		GetWorld()->SpawnActor<AOkSpawnDeathMesh>(SpawnDeathMesh, GetMesh()->GetComponentLocation(), GetMesh()->GetComponentRotation())->SetMaterial_Multicast(CollorTeamOne);
	}
	else
	{
		GetWorld()->SpawnActor<AOkSpawnDeathMesh>(SpawnDeathMesh, GetMesh()->GetComponentLocation(), GetMesh()->GetComponentRotation())->SetMaterial_Multicast(CollorTeamTwo);
	}
	SetScore_Multicast();
}

void AOKBaseCharacter::SetDeath_Multicast_Implementation()
{
	if (HasAuthority())
	{
		CachedBasePlayerState = Cast<AOKPlayerState>(GetPlayerState());
		CachedBasePlayerState.Get()->SetDeath_OnServer();
	}
	else
	{
		CachedBasePlayerState = Cast<AOKPlayerState>(GetPlayerState());
		CachedBasePlayerState.Get()->SetDeath_Multicast();
	}
}




	//CachedBaseController = Cast<AOKBasePlayerController>(GetController());
	//CurrentController = CachedBaseController.Get();


void AOKBaseCharacter::SetCurrentController(AOKBasePlayerController* NewCurrentController)
{
	//CachedBaseController = Cast<AOKBasePlayerController>(GetController());
	CurrentController = NewCurrentController;
}

void AOKBaseCharacter::SetPlayerToPlayersGameMode()
{
	if (CurrenTrueCollor == CollorGM)
	{
		CachedBaseGameMode = Cast<AOneKillGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		CachedBaseGameMode.Get()->SetPlayersToCommandRed(this);
	}
	else
	{
		CachedBaseGameMode = Cast<AOneKillGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		CachedBaseGameMode.Get()->SetPlayersToCommandBlue(this);
	}
	
}

void AOKBaseCharacter::CreateWeapon(AOKBasePlayerController* WeaponController)
{
	if (CurrentNameWeapon == "")
	{
		CurrentNameWeapon = "Rifle_v1";
		InitWeapon(CurrentNameWeapon,Info, 0);
		WeaponController->SetCurrentAmmoWeapon_Multicast(GetCurrentWeapon()->AdditionalWeaponInfo.Round, GetCurrentWeapon());
		SetPlayerToPlayersGameMode();
	}
	else
	{
		InitWeapon(CurrentNameWeapon,Info, 0);
		WeaponController->SetCurrentAmmoWeapon_Multicast(GetCurrentWeapon()->AdditionalWeaponInfo.Round, GetCurrentWeapon());
		SetPlayerToPlayersGameMode();
		//CreateNewWeaponBP();
	}
}

bool AOKBaseCharacter::GetIsReloading()
{
	return bIsReloading;
}

void AOKBaseCharacter::CreateNewWeaponBP_Implementation()
{
	
}

void AOKBaseCharacter::IniteWeapon_OnServer_Implementation(const FString& WeaponIdName,
                                                           const FAdditionalWeaponInfo& AdditionalWeaponInfo)
{
	
}


bool AOKBaseCharacter::CanSprint()
{
	return true;
}

void AOKBaseCharacter::AnimFireAnim_Multicast_Implementation(UAnimMontage* AnimFireChar,UAnimMontage* AnimFireFPSChar,UAnimMontage* AnimFireWeaponFPS, UAnimMontage* AnimFireWeapon)
{
	PlayAnimMontage(AnimFireChar);
	FirstPersonMeshComponent->GetAnimInstance()->Montage_Play(AnimFireFPSChar);
	FirstPersonMeshWeaponComponent->GetAnimInstance()->Montage_Play(AnimFireWeaponFPS);
}
void AOKBaseCharacter::FireAnim_Multicast_Implementation(UAnimMontage* AnimFireChar,UAnimMontage* AnimFireFPSChar,UAnimMontage* AnimFireWeaponFPS, UAnimMontage* AnimFireWeapon)
{
	PlayAnimMontage(AnimFireChar);
	FirstPersonMeshComponent->GetAnimInstance()->Montage_Play(AnimFireFPSChar);
	FirstPersonMeshWeaponComponent->GetAnimInstance()->Montage_Play(AnimFireWeaponFPS);
	MeshWeaponComponent->GetAnimInstance()->Montage_Play(AnimFireWeapon);
}
void AOKBaseCharacter::CharRespawn_Implementation()
{
	bIsLive = true;
}

void AOKBaseCharacter::ReloadAnim_Multicast_Implementation(UAnimMontage* AnimReload, UAnimMontage* FPSAnimReload, UAnimMontage* FPSAnimReloadWeapon, UAnimMontage* AnimReloadWeapon)
{
	PlayAnimMontage(AnimReload);
	FirstPersonMeshComponent->GetAnimInstance()->Montage_Play(FPSAnimReload);
	FirstPersonMeshWeaponComponent->GetAnimInstance()->Montage_Play(FPSAnimReloadWeapon);
	MeshWeaponComponent->GetAnimInstance()->Montage_Play(AnimReloadWeapon);
}

void AOKBaseCharacter::SetMaxWallkSpeed_Multicast_Implementation(float Speed)
{
	GetCharacterMovement()->MaxWalkSpeed = Speed;
}


void AOKBaseCharacter::SetSprintRequested_OnServer_Implementation(bool Value)
{
	bIsSprintRequested = Value;

}

void AOKBaseCharacter::SetIsSprinting_OnServer_Implementation(bool Sprint)
{
	bIsSprinting = Sprint;

}
void AOKBaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOKBaseCharacter, CurrentWeapon);
	DOREPLIFETIME(AOKBaseCharacter, bIsAiming);
	DOREPLIFETIME(AOKBaseCharacter, bIsSprinting);
	DOREPLIFETIME(AOKBaseCharacter, bIsSprintRequested);
	DOREPLIFETIME(AOKBaseCharacter, SprintSpeed);
	DOREPLIFETIME(AOKBaseCharacter, CurrenTrueCollor);
	DOREPLIFETIME(AOKBaseCharacter, CurrentName);
	DOREPLIFETIME(AOKBaseCharacter, CurrentNameWeapon);
	DOREPLIFETIME(AOKBaseCharacter, CurrentPlayerState);
	DOREPLIFETIME(AOKBaseCharacter, CachedBasePlayerState);
	DOREPLIFETIME(AOKBaseCharacter, CurrentIdPlayer);
	DOREPLIFETIME(AOKBaseCharacter, CurrentCollor);
	//DOREPLIFETIME(AOKBaseCharacter, CachedBaseController);
	DOREPLIFETIME(AOKBaseCharacter, CurrentController);
}








