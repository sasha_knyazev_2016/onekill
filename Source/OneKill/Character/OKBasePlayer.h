// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "OKBaseCharacter.h"
#include "OKBasePlayer.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class ONEKILL_API AOKBasePlayer : public AOKBaseCharacter
{
	GENERATED_BODY()


public:
	AOKBasePlayer();

	virtual void BeginPlay() override;
	virtual void MoveForward(float Value) override;
	virtual void MoveRight(float Value) override;
	virtual void Turn(float Value) override;
	virtual void LookUp(float Value) override;
	virtual void Aiming() override;
	virtual void DeaAiming() override;
	
	UFUNCTION(BlueprintCallable)
	void CreateMeshWeapon();

	bool bIsAttach;
	
	virtual void TryReload() override;
	virtual void StartAttack() override;
	virtual void StopAttack() override;
	virtual void AttackCharEvent(bool bIsFiring) override;

	virtual void WeaponReloadStart(UAnimMontage* AnimReload, UAnimMontage* FPSAnimReload,  UAnimMontage* FPSAnimReloadWeapon, UAnimMontage* AnimReloadWeapon) override;
	virtual void WeaponReloadEnd(bool Success, int32 AmmoSafe) override;

	virtual void SwitchNextWeapon() override;
	virtual void SwitchPreviosWeapon() override;
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Camera")
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | Camera")
	USpringArmComponent* SpringArmComponent;
	
	UFUNCTION(BlueprintNativeEvent)
	void Aiming_BP();
	UFUNCTION(BlueprintNativeEvent)
	void DeaAiming_BP();

};
