// Fill out your copyright notice in the Description page of Project Settings.


#include "OKSpectatorPawn.h"

#include "OneKill/Controller/OKBasePlayerController.h"

void AOKSpectatorPawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
}
void AOKSpectatorPawn::BeginPlay()
{
	Super::BeginPlay();
}

void AOKSpectatorPawn::SetMyTransform(AOKBaseCharacter* CharStalker)
{
	if (IsValid(CharStalker))
	{	
		this->SetActorTransform(CharStalker->GetActorTransform(), false);
	}
}

TArray<AOKBaseCharacter*> AOKSpectatorPawn::GetCurrentPlayersStalker(TArray<AOKBaseCharacter*> AllPlayers, bool& IsAllDead)
{
	IsAllDead = true;
	TArray<AOKBaseCharacter*> IsNotDeadPlayers;
	for (int i = 0; i <= AllPlayers.Num()-1; i++)
	{
		if (!AllPlayers[i]->bIsDead)
		{
			IsNotDeadPlayers.Add(AllPlayers[i]);
			IsAllDead = false;
		}
	}
	
	return IsNotDeadPlayers;
}

void AOKSpectatorPawn::GetCurrentPriviosPlayerToSpectate()
{
	if (CurrentSpectatorPlayer - 1 == -1)
	{
		CurrentSpectatorPlayer = PlayersCollor.Num()-1;
		if (!PlayersCollor[CurrentSpectatorPlayer]->bIsDead)
		{
				PVSpectatePriviosPlayer_BP(PlayersCollor[CurrentSpectatorPlayer]);
		}
	}
	else 
	{
		CurrentSpectatorPlayer = CurrentSpectatorPlayer-1;
		if (!PlayersCollor[CurrentSpectatorPlayer]->bIsDead)
		{
				PVSpectatePriviosPlayer_BP(PlayersCollor[CurrentSpectatorPlayer]);
		}
	}
}

void AOKSpectatorPawn::GetCurrentNextPlayerToSpectate()
{
	if (PlayersCollor.Num()+ 1 == CurrentSpectatorPlayer + 1)
	{
		CurrentSpectatorPlayer = 0;
		if (!PlayersCollor[CurrentSpectatorPlayer]->bIsDead)
		{
			NXSpectateNextPlayer_BP(PlayersCollor[CurrentSpectatorPlayer]);
		}
	}
	else
	{
		CurrentSpectatorPlayer = CurrentSpectatorPlayer + 1;
		if (!PlayersCollor[CurrentSpectatorPlayer]->bIsDead)
		{
			NXSpectatePriviosPlayer_BP(PlayersCollor[CurrentSpectatorPlayer]);
		}
	}
}

void AOKSpectatorPawn::SetMyTransform_Multicast_Implementation(AOKBaseCharacter* CharStalker)
{
	SetMyTransform(CharStalker);
}

void AOKSpectatorPawn::NXSpectatePriviosPlayer_BP_Implementation(AOKBaseCharacter* PlayerSpaectate)
{
	
}

void AOKSpectatorPawn::NXSpectateNextPlayer_BP_Implementation(AOKBaseCharacter* PlayerSpaectate)
{
	
}

void AOKSpectatorPawn::PVSpectatePriviosPlayer_BP_Implementation(AOKBaseCharacter* PlayerSpaectate)
{
	
}

void AOKSpectatorPawn::PVSpectateNextPlayer_BP_Implementation(AOKBaseCharacter* PlayerSpaectate)
{
	
}


void AOKSpectatorPawn::SpectateNextPlayer_BP_Implementation(const TArray<AOKBaseCharacter*>& Players,
                                                            AOKBasePlayerController* ControllerSpaectator)
{
	
}

void AOKSpectatorPawn::SpectatePriviousPlayer_BP_Implementation(const TArray<AOKBaseCharacter*>& Players,
                                                                AOKBasePlayerController* ControllerSpaectator)
{
	
}


void AOKSpectatorPawn::SpectateNextPlayer_Multicast_Implementation(const TArray<AOKBaseCharacter*>& Players,
                                                                   AOKBasePlayerController* ControllerSpaectator)
{
	GetCurrentPlayers_OnServer(Players);
	SpectateNextPlayer_BP(Players, ControllerSpaectator);
	
}

void AOKSpectatorPawn::SpectatePriviousPlayer_Multicast_Implementation(const TArray<AOKBaseCharacter*>& Players,
                                                                       AOKBasePlayerController* ControllerSpaectator)
{
	GetCurrentPlayers_OnServer(Players);
	SpectatePriviousPlayer_BP(Players, ControllerSpaectator);
	//GetCurrentPriviosPlayerToSpectate();
}

void AOKSpectatorPawn::GetCurrentPlayers_OnServer_Implementation(
	const TArray<AOKBaseCharacter*>& AllPlayers)
{
	GetCurrentPlayersStalker(AllPlayers, bIsAllDead);
}

void AOKSpectatorPawn::SetInfoBySpectator_Multicast_Implementation(AOKBasePlayerController* SpectatorController, AOKBaseCharacter* CharStalker)
{
	SpectatorController->SetHealthToSpectator_Multicast(CharStalker);
	SpectatorController->SetArmorToSpectator_Multicast(CharStalker);
}

void AOKSpectatorPawn::SetActorLocation_BP_Multicast_Implementation()
{
	
}

void AOKSpectatorPawn::SetActorLocation_Multicast_Implementation(AOKBaseCharacter* CharStalker)
{
	//SetActorLocation_BP_Multicast_Implementation();
	SetMyTransform(CharStalker);
	
	
}

void AOKSpectatorPawn::GetLifetimeReplicatedProps(TArray<class FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOKSpectatorPawn, PlayerCollor);
	DOREPLIFETIME(AOKSpectatorPawn, bIsAllDead);
	DOREPLIFETIME(AOKSpectatorPawn, PlayerStalker);
	DOREPLIFETIME(AOKSpectatorPawn, BaseCharStalker);
	DOREPLIFETIME(AOKSpectatorPawn, PlayersCollor);
	
}
