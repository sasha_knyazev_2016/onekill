// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/MaterialInterface.h"
#include "OkSpawnDeathMesh.generated.h"

UCLASS()
class ONEKILL_API AOkSpawnDeathMesh : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOkSpawnDeathMesh();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	class USkeletalMeshComponent* SkeletalMeshComponent = nullptr;

	FTimerHandle TimerHandleDestroyTimer;

	UFUNCTION()
	void DestroyTimer();
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Anim")
	UAnimMontage* DeathMontage;

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetMaterial_Multicast(UMaterialInterface* Collor);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
