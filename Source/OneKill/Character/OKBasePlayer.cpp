// Fill out your copyright notice in the Description page of Project Settings.


#include "OKBasePlayer.h"

AOKBasePlayer::AOKBasePlayer()
{
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;


	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->bUsePawnControlRotation = true;


	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);
	CameraComponent->bUsePawnControlRotation = false;

	GetCharacterMovement()->bOrientRotationToMovement = 1;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
}

void AOKBasePlayer::BeginPlay()
{
	Super::BeginPlay();
}

void AOKBasePlayer::MoveForward(float Value)
{
	if (!FMath::IsNearlyZero(Value, 1e-6f))
	{
		FRotator YawRotator(0.0f, GetControlRotation().Yaw, 0.0f);
		FVector ForwardVector = YawRotator.RotateVector(FVector::ForwardVector);
		AddMovementInput(ForwardVector, Value);
	}
}

void AOKBasePlayer::MoveRight(float Value)
{
	if (!FMath::IsNearlyZero(Value, 1e-6f))
	{
		FRotator YawRotator(0.0f, GetControlRotation().Yaw, 0.0f);
		FVector RightVector = YawRotator.RotateVector(FVector::RightVector);
		AddMovementInput(RightVector, Value);
	}
}

void AOKBasePlayer::Turn(float Value)
{
	AddControllerYawInput(Value);
}

void AOKBasePlayer::LookUp(float Value)
{
	AddControllerPitchInput(Value);
}

void AOKBasePlayer::Aiming()
{
	Super::Aiming();
	if (!GetIsReloading())
	{
		FirstPersonCameraComponent->SetWorldLocation(FirstPersonMeshWeaponComponent->GetSocketLocation("AimSoket"));
	}
	Aiming_BP();
}

void AOKBasePlayer::DeaAiming()
{
	Super::DeaAiming();
	FirstPersonCameraComponent->SetWorldLocation(FirstPersonArrowComponent->GetComponentLocation());
	DeaAiming_BP();
}

void AOKBasePlayer::CreateMeshWeapon()
{
	AOKBaseWeapon* myWeapon = GetCurrentWeapon();
	//MeshWeaponComponent->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepWorldTransform, "WeaponSoketRightHand");
	//MeshWeaponComponent->SetSkeletalMeshAsset(myWeapon->CurrentSkeletalMesh);



	FirstPersonMeshWeaponComponent->AttachToComponent(FirstPersonMeshComponent, FAttachmentTransformRules::KeepWorldTransform, "WeaponSoketRightHand");
	FirstPersonMeshWeaponComponent->SetSkeletalMeshAsset(myWeapon->CurrentSkeletalMesh);
	//myWeapon->SetSkeletalMesh_Multicast_Implementation();

	FirstPersonMeshWeaponComponent->SetOnlyOwnerSee(true);
	//MeshWeaponComponent->SetOwnerNoSee(true);

	//MeshWeaponComponent->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepWorldTransform, "WeaponSoketRightHand");
	//MeshWeaponComponent->SetSkeletalMeshAsset(GetCurrentWeapon()->CurrentSkeletalMesh);

	
	//FirstPersonMeshWeaponComponent->AttachToComponent(FirstPersonMeshComponent,FAttachmentTransformRules::KeepWorldTransform, "WeaponSoketRightHand");
	//FirstPersonMeshWeaponComponent->SetSkeletalMeshAsset(GetCurrentWeapon()->CurrentSkeletalMesh);
	//GetCurrentWeapon()->SetSkeletalMesh_Multicast_Implementation();
	//FirstPersonMeshWeaponComponent->SetOnlyOwnerSee(true);
}

void AOKBasePlayer::TryReload()
{
	Super::TryReload();
}

void AOKBasePlayer::StartAttack()
{
	Super::StartAttack();
}

void AOKBasePlayer::StopAttack()
{
	Super::StopAttack();
}

void AOKBasePlayer::AttackCharEvent(bool bIsFiring)
{
	Super::AttackCharEvent(bIsFiring);
}

void AOKBasePlayer::WeaponReloadStart(UAnimMontage* AnimReload, UAnimMontage* FPSAnimReload,  UAnimMontage* FPSAnimReloadWeapon, UAnimMontage* AnimReloadWeapon)
{
	Super::WeaponReloadStart(AnimReload,FPSAnimReload, FPSAnimReloadWeapon, AnimReloadWeapon);
}

void AOKBasePlayer::WeaponReloadEnd(bool Success, int32 AmmoSafe)
{
	Super::WeaponReloadEnd(Success,AmmoSafe);
}

void AOKBasePlayer::SwitchNextWeapon()
{
	Super::SwitchNextWeapon();
}

void AOKBasePlayer::SwitchPreviosWeapon()
{
	Super::SwitchPreviosWeapon();
}

void AOKBasePlayer::Aiming_BP_Implementation()
{
}

void AOKBasePlayer::DeaAiming_BP_Implementation()
{
}
