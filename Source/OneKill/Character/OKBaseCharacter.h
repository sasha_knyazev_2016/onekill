// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../Component/OKInventoryComponent.h"
#include "../Component/OKHealthCharacterComponent.h"
#include"../Game/OKBaseGameInstance.h"
#include"../Weapon/OKBaseWeapon.h"
#include "../OKBaseCommandLocation.h"
#include "Materials/MaterialInterface.h"
#include "../Interface/ControllerGameMode.h"
#include "../Game/OKPlayerState.h"
#include "../Game/OneKillGameModeBase.h"
#include "OkSpawnDeathMesh.h"
#include "GameFramework/Character.h"
#include "OKBaseCharacter.generated.h"


class AOKBasePlayerController;


UCLASS(Abstract, NotBlueprintable)
class ONEKILL_API AOKBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	AOKBaseCharacter();

	
	

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Command")
	bool bIsCommandTarget;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UOKInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UOKHealthCharacterComponent* HealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | First person")
	class USkeletalMeshComponent* FirstPersonMeshComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | First person")
	class USkeletalMeshComponent* FirstPersonMeshWeaponComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | First person")
	class USkeletalMeshComponent* MeshWeaponComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | First person")
	class UCameraComponent* FirstPersonCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | First person")
	class UArrowComponent* FirstPersonArrowComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character | First person")
	FName SoketFPCamera;

	

	virtual void BeginPlay() override;
	

	virtual void MoveForward(float Value) {};
	virtual void MoveRight(float Value) {};
	virtual void Turn(float Value) {};
	virtual void LookUp(float Value) {};
	virtual void Aiming();
	virtual void DeaAiming();
	UFUNCTION(Server, Reliable)
	void SetAiming_OnServer(bool Aim);

	virtual void ChangeCrouchState();

	virtual void StartSprint();
	virtual void StopSprint();
	
	virtual void StartAttack();
	virtual void StopAttack();

	virtual void AttackCharEvent(bool bIsFiring);
	virtual void TryReload();

	virtual void SwitchNextWeapon();
	virtual void SwitchPreviosWeapon();
	void ActionWeaponSlot1();
	void ActionWeaponSlot2();
	void Shoot();
	UFUNCTION(BlueprintNativeEvent)
	void Shoot_BP();
	void ScoreBoard();
	UFUNCTION(BlueprintNativeEvent)
	void ScoreBoardActive_BP();
	UFUNCTION(BlueprintNativeEvent)
	void ScoreBoardDeactive_BP();
	bool bISActiveScoreBoard = false;



	UFUNCTION(BlueprintNativeEvent)
	void ActionWeaponSlot1_BP();
	UFUNCTION(BlueprintNativeEvent)
	void ActionWeaponSlot2_BP();
	UFUNCTION(BlueprintNativeEvent)
	void TryReloadWidget_BP();

	UFUNCTION()
	virtual void WeaponReloadStart(UAnimMontage* AnimReload, UAnimMontage* FPSAnimReload,  UAnimMontage* FPSAnimReloadWeapon, UAnimMontage* AnimReloadWeapon);
	UFUNCTION()
	virtual void WeaponReloadEnd(bool Success, int32 AmmoSafe);
	UFUNCTION()
	void WeaponFireAnim(UAnimMontage* AnimFireChar,UAnimMontage* AnimFireFPSChar,UAnimMontage* AnimFireWeaponFPS, UAnimMontage* AnimFireWeapon, UAnimMontage* AnimFireAnim);
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;
	virtual float TakeDamage(float DamageAmount, struct  FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	void CharDead();

	virtual void Tick(float DeltaTime);

	FRotator CustomRotationAnim;

	bool GetIsAiming();
	bool GetIsLive();
	FTimerHandle TimerHandleRagDollTimer;
	void EnableRagdoll();
	FORCEINLINE bool GetIsSprinting() { return bIsSprinting; }

	//Weapon
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName WeaponIdName, FAdditionalWeaponInfo WeaponAdditionalWeaponInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintPure)
	FWeaponInfo GetWeaponImfo();
	FWeaponInfo CurrentweaponInfo;
	UPROPERTY(Replicated, BlueprintReadWrite)
	AOKBaseWeapon* CurrentWeapon = nullptr;
	UFUNCTION(BlueprintCallable)
	AOKBaseWeapon* GetCurrentWeapon();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;

	UFUNCTION(Server, Reliable)
	void TryReload_OnServer();
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Weapon")
	bool bIsAiming = false;

	
	UFUNCTION(BlueprintCallable)
	FVector SetCharacterToCommandLocation(int IndexCommand);

	UFUNCTION(BlueprintCallable)
	FVector SetCharacterToCommandLocationToId(int IndexCommand);
	TSubclassOf<AOKBaseCommandLocation> BaseCommandLocation;
	
	
	
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void GetPlayerInfoToNetworkBeginPlay_Multicast(APlayerState* CurrentState);
	FTimerHandle BeginPlayMulticastTimer;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	AOKPlayerState* CurrentPlayerState;

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetCurrentNameFromLobby_Multicast();
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetCurrentWeaponForLobby_Multicast();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetCollor_OnServer();
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetCollor_Multicast(UMaterialInterface* NewMaterial);
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	UMaterialInterface* CurrentCollor;
	UFUNCTION()
	void SetTrueCollor(int Id);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void PlayHitMontage_Multicast(AOKBaseCharacter* DamegeChar);


	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetScore_Multicast();

	UFUNCTION(BlueprintNativeEvent)
	void SetScoreBP();

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetKill_Multicast();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character | Network")
	int CurrentMyKills;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	UAnimMontage* AnimationHit;


	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	FName CurrentName;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Character | Network")
	FName CurrentNameWeapon;
	
	TArray<AActor*> CollorsCharacters;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	int CurrentIdPlayer;

	UPROPERTY(Replicated, EditAnywhere, Category = "Character | Network")
	TSoftObjectPtr<AOKPlayerState> CachedBasePlayerState;


	TSoftObjectPtr<AOneKillGameModeBase> CachedBaseGameMode;
	TSoftObjectPtr<AOKBaseCharacter> CachedBasePlayers;
	UPROPERTY(EditAnywhere, Category = "Character | Network")
	TSoftObjectPtr<AOKBasePlayerController> CachedBaseController;
	TSoftObjectPtr<UOKBaseGameInstance> CachedBaseGameInstance;
	UPROPERTY(Replicated,BlueprintReadWrite, EditAnywhere, Category = "Character | Network")
	AOKBasePlayerController* CurrentController;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Info")
	UMaterialInterface* CollorTeamOne;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Info")
	UMaterialInterface* CollorTeamTwo;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	UMaterialInterface* CurrenTrueCollor;

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void CharRespawn_Multicast();

	UFUNCTION(BlueprintCallable)
	void SetCurrentController(class AOKBasePlayerController* NewCurrentController);

	UPROPERTY(EditAnywhere)
	TSubclassOf<AOkSpawnDeathMesh> SpawnDeathMesh;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	int MyCurrentTeam;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	bool bIsDead;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	TArray<AActor*> FoundEnemies;

	



	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetDeath_Multicast();
	UFUNCTION(BlueprintCallable)
	void SetPlayerToPlayersGameMode();
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Network")
	UMaterialInterface* CollorGM;

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void IniteWeapon_OnServer(const FString& WeaponIdName, const FAdditionalWeaponInfo& AdditionalWeaponInfo);
	UFUNCTION(BlueprintCallable)
	void CreateWeapon(AOKBasePlayerController* WeaponController);
	UFUNCTION(BlueprintNativeEvent)
	void CreateNewWeaponBP();
	
protected:
	UFUNCTION(BlueprintCallable)
	bool GetIsReloading();


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Weapon")
	FAdditionalWeaponInfo Info;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Movement")
	float SprintSpeed = 800.0f;

	float SpeedAiming = 400.0f;
	
	virtual bool CanSprint();
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Anim")
	TArray<UAnimMontage*> DeathMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character | Anim")
	UAnimMontage* SwitchWeaponhMontage;
	
	UFUNCTION(NetMulticast, Unreliable)
	void AnimFireAnim_Multicast(UAnimMontage* AnimFireChar,UAnimMontage* AnimFireFPSChar,UAnimMontage* AnimFireWeaponFPS, UAnimMontage* AnimFireWeapon);
	UFUNCTION(NetMulticast, Unreliable)
	void FireAnim_Multicast(UAnimMontage* AnimFireChar,UAnimMontage* AnimFireFPSChar,UAnimMontage* AnimFireWeaponFPS, UAnimMontage* AnimFireWeapon);
	UFUNCTION(NetMulticast, Unreliable)
	void ReloadAnim_Multicast(UAnimMontage* AnimReload, UAnimMontage* FPSAnimReload,  UAnimMontage* FPSAnimReloadWeapon, UAnimMontage* AnimReloadWeapon);

	UFUNCTION(BlueprintNativeEvent)
	void CharRespawn();

	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Movement")
	bool bIsSprinting = false;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | Anim");
	bool bIsSprintRequested;
	
private:
	

	bool bIsReloading;

	float DefaultMaxMovementSpeed = 600.0f;
	UFUNCTION(Server, Unreliable)
	void SetSprintRequested_OnServer(bool Value);
	UFUNCTION(Server, Unreliable)
	void SetIsSprinting_OnServer(bool Sprint);
	UFUNCTION(NetMulticast, Reliable)
	void SetMaxWallkSpeed_Multicast(float Speed);

	bool bIsLive = true;
};
