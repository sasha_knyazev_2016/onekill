// Fill out your copyright notice in the Description page of Project Settings.


#include "OKBasePlayerController.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"


void AOKBasePlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	CachedBaseCharacter = Cast<AOKBaseCharacter>(InPawn);
	//CachedBaseCharacter.Get()->SetCurrentAmmoByWiget(this);
	CachedBaseGameMode = Cast<class AOneKillGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	
}

void AOKBasePlayerController::SetCurrentAmmoWeapon_Multicast_BP_Implementation(int Ammo,
	AOKBaseWeapon* Weapon)
{
	
}

void AOKBasePlayerController::SetCurrentAmmoWeapon_Multicast_Implementation(int Ammo, AOKBaseWeapon* Weapon)
{
	if (this->IsLocalController())
	{
		SetCurrentAmmoWeapon_Multicast_BP(Ammo, Weapon);
	}
}


void AOKBasePlayerController::SetScoreToCommand_Multicast_Implementation(int NewScore, int CurrentScoreWar, int Id)
{
	if (Id % 2)
	{
		SetScoreToCommandOne(NewScore);
		SetScoreToCommandTwo(CurrentScoreWar);
	}
	else
	{
		SetScoreToCommandTwo(NewScore);
		SetScoreToCommandOne(CurrentScoreWar);
	}
}

void AOKBasePlayerController::SetScoreToCommand_OnServer_Implementation(int NewScore, int CurrentScoreWar, int Id)
{
	SetScoreToCommand_Multicast(NewScore, CurrentScoreWar, Id);
}


void AOKBasePlayerController::SetScoreToCommandOne_Implementation(int NewScore)
{

}

void AOKBasePlayerController::SetScoreToCommandTwo_Implementation(int NewScored)
{

}

void AOKBasePlayerController::SetImageToCommand_Multicast_Implementation(int NewCommand)
{
	SetImageToCommandBP(NewCommand);
}

void AOKBasePlayerController::SetImageToCommandBP_Implementation(int NewCommand)
{

}

void AOKBasePlayerController::SetScoreToCommandSpectator_Multicast_Implementation(int NewScore, int Id)
{
	if (Id % 2)
	{
		SetScoreToCommandOneSpectatorBP(NewScore);
	}
	else
	{
		SetScoreToCommandTwoSpectatorBP(NewScore);
	}
}

void AOKBasePlayerController::SetScoreToCommandSpectator_OnServer_Implementation(int NewScore, int Id)
{
	SetScoreToCommandSpectator_Multicast(NewScore, Id);
}


void AOKBasePlayerController::SetScoreToCommandOneSpectatorBP_Implementation(int NewScore)
{
}

void AOKBasePlayerController::SetScoreToCommandTwoSpectatorBP_Implementation(int NewScored)
{
}

void AOKBasePlayerController::SetIdPlayerStalker_Multicast_Implementation(int NewId)
{
	CurrentIdStalker = NewId;
}


void AOKBasePlayerController::SetNameToSpectator_Multicast_Implementation(FName Name)
{
	SetNameToSpectatorBP(Name);
}


void AOKBasePlayerController::SetNameToSpectatorBP_Implementation(FName Name)
{
}

void AOKBasePlayerController::SetScoreToCommandSpectatorStart_Multicast_Implementation(int ScoreComandOne, int ScoreComandTwo)
{
	SetScoreToCommandSpectatorStartBP(ScoreComandOne, ScoreComandTwo);
}

void AOKBasePlayerController::SetScoreToCommandSpectatorStartBP_Implementation(int ScoreComandOne, int ScoreComandTwo)
{

}

void AOKBasePlayerController::SetCurrentAmmoBySpectator_Multicast_Implementation(AOKBaseWeapon* Weapon, int Id)
{
	if (IsLocalController() && Id == CurrentIdStalker)
	{
		SetCurrentAmmoBySpectatorBP(Weapon, Id);
	}	
}

void AOKBasePlayerController::SetCurrentAmmoBySpectatorBP_Implementation(AOKBaseWeapon* Weapon, int Id)
{

}

void AOKBasePlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOKBasePlayerController, CurrentIdStalker);
	DOREPLIFETIME(AOKBasePlayerController, CachedBaseCharacter);
}

void AOKBasePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveForward", this, &AOKBasePlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AOKBasePlayerController::MoveRight);
	InputComponent->BindAxis("Turn", this, &AOKBasePlayerController::Turn);
	InputComponent->BindAxis("LookUp", this, &AOKBasePlayerController::LookUp);
	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::Jump);
	InputComponent->BindAction("Aiming", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::Aiming);
	InputComponent->BindAction("Aiming", EInputEvent::IE_Released, this, &AOKBasePlayerController::DeaAiming);
	InputComponent->BindAction("Crouch", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::ChangeCrouchState);
	InputComponent->BindAction("Sprint", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::StartSprint);
	InputComponent->BindAction("Sprint", EInputEvent::IE_Released, this, &AOKBasePlayerController::StopSprint);
	InputComponent->BindAction("FireEvent", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::StartAttack);
	InputComponent->BindAction("FireEvent", EInputEvent::IE_Released, this, &AOKBasePlayerController::StopAttack);
	InputComponent->BindAction("ReloadEvent", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::TryReload);
	InputComponent->BindAction("SwitchNextWeapon", EInputEvent::IE_Released, this, &AOKBasePlayerController::SwitchNextWeapon);
	InputComponent->BindAction("SwitchPreviosWeapon", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::SwitchPreviosWeapon);
	InputComponent->BindAction("ActionWeaponSlot1", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::ActionWeaponSlot1);
	InputComponent->BindAction("ActionWeaponSlot2", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::ActionWeaponSlot2);
	InputComponent->BindAction("ScorBoard", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::ScoreBoard);
	InputComponent->BindAction("ScorBoard", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::ScoreBoard);
	InputComponent->BindAction("SwitchPlayerSpectatorNext", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::SwitchPlayerSpectatorNext);
	InputComponent->BindAction("SwitchPlayerSpectatorPrivious", EInputEvent::IE_Pressed, this, &AOKBasePlayerController::SwitchPlayerSpectatorPrivious);

}

void AOKBasePlayerController::MoveForward(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->MoveForward(Value);
	}
}

void AOKBasePlayerController::MoveRight(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->MoveRight(Value);
	}
}

void AOKBasePlayerController::Turn(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->Turn(Value);
	}
}

void AOKBasePlayerController::LookUp(float Value)
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->LookUp(Value);
	}
}

void AOKBasePlayerController::Jump()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->Jump();
	}
}

void AOKBasePlayerController::Aiming()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->Aiming();
	}
}

void AOKBasePlayerController::DeaAiming()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->DeaAiming();
	}
}

void AOKBasePlayerController::ChangeCrouchState()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->ChangeCrouchState();
	}
}

void AOKBasePlayerController::StartSprint()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->StartSprint();
	}
}

void AOKBasePlayerController::StopSprint()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->StopSprint();
	}
}

void AOKBasePlayerController::StartAttack()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->StartAttack();
	}
}

void AOKBasePlayerController::StopAttack()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->StopAttack();
	}
}

void AOKBasePlayerController::TryReload()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->TryReload();
	}
}

void AOKBasePlayerController::SwitchNextWeapon()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->SwitchNextWeapon();
	}
}

void AOKBasePlayerController::SwitchPreviosWeapon()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->SwitchPreviosWeapon();
	}
}

void AOKBasePlayerController::ActionWeaponSlot1()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->ActionWeaponSlot1();
	}
}

void AOKBasePlayerController::ActionWeaponSlot2()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->ActionWeaponSlot2();
	}
}

void AOKBasePlayerController::ScoreBoard()
{
	if (CachedBaseCharacter.IsValid())
	{
		CachedBaseCharacter->ScoreBoard();
	}
}

void AOKBasePlayerController::SwitchPlayerSpectatorNext()
{
	//GetPawn()
	SwitchPlayerSpectatorNext_BP();
}

void AOKBasePlayerController::SwitchPlayerSpectatorPrivious()
{
	SwitchPlayerSpectatorPrivious_BP();
}

int AOKBasePlayerController::GetCurrentPlayerId(AOKBaseCharacter* PlayerStalker)
{
	return PlayerStalker->GetPlayerState()->PlayerId;
}

void AOKBasePlayerController::SwitchPlayerSpectatorNext_BP_Implementation()
{
}

void AOKBasePlayerController::SwitchPlayerSpectatorPrivious_BP_Implementation()
{
}

void AOKBasePlayerController::SetArmorToSpectator_Multicast_Implementation(AOKBaseCharacter* PlayerStalker)
{
	if (CurrentIdStalker == GetCurrentPlayerId(PlayerStalker))
	{
		SetArmorToSpectatorBP(PlayerStalker);
	}
}

void AOKBasePlayerController::SetArmorToSpectatorBP_Implementation(AOKBaseCharacter* PlayerStalker)
{
}

void AOKBasePlayerController::SetHealthToSpectator_Multicast_Implementation(AOKBaseCharacter* PlayerStalker)
{
	if (CurrentIdStalker == PlayerStalker->CurrentIdPlayer)
	{
		SetHealthToSpectatorBP(PlayerStalker);
	}
}

void AOKBasePlayerController::SetHealthToSpectatorBP_Implementation(AOKBaseCharacter* PlayerStalker)
{
	
}

void AOKBasePlayerController::StartPlay_Multicast_Implementation()
{
	SetScoreToCommand_OnServer(0,0,0);
	SetScoreToCommand_OnServer(0,0,1);
}

void AOKBasePlayerController::EndGameWin_OnServer_Implementation(int ScoreOne, int ScoreTwo, FName NameTeamWin, UObject* UiIconTeamWin)
{
	EndGameWin_Multicast(ScoreOne, ScoreTwo, NameTeamWin, UiIconTeamWin);
}

void AOKBasePlayerController::EndGameWin_Multicast_Implementation(int ScoreOne, int ScoreTwo, FName NameTeamWin, UObject* UiIconTeamWin)
{
	EndGameWin_BP_Implementation(ScoreOne, ScoreTwo, NameTeamWin, UiIconTeamWin);
}

void AOKBasePlayerController::EndGameWin_BP_Implementation(int ScoreOne, int ScoreTwo, FName NameTeamWin, UObject* UiIconTeamWin)
{

}

void AOKBasePlayerController::SetScoreToRespawn_OnServer_Implementation(int ScoreOne, int ScoreTwo)
{
	SetScoreToRespawn_Multicast(ScoreOne, ScoreTwo);
}

void AOKBasePlayerController::SetScoreToRespawn_Multicast_Implementation(int ScoreOne, int ScoreTwo)
{
	SetScoreToRespawn_BP(ScoreOne, ScoreTwo);
}

void AOKBasePlayerController::SetScoreToRespawn_BP_Implementation(int ScoreOne, int ScoreTwo)
{

}


