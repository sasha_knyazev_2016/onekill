// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Character/OKBaseCharacter.h"
#include "GameFramework/PlayerController.h"
#include "../Interface/ControllerGameMode.h"
#include "OKBasePlayerController.generated.h"

class AOneKillGameModeBase;


/**
 * 
 */
UCLASS()
class ONEKILL_API AOKBasePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void SetPawn(APawn* InPawn) override;
	
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetScoreToCommand_OnServer(int NewScore, int CurrentScoreWar,  int Id);
	UFUNCTION(NetMulticast, Reliable)
	void SetScoreToCommand_Multicast(int NewScore, int CurrentScoreWar, int Id);
	UFUNCTION(BlueprintNativeEvent)
	void SetScoreToCommandOne(int NewScore);
	UFUNCTION(BlueprintNativeEvent)
	void SetScoreToCommandTwo(int NewScored);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetImageToCommand_Multicast(int NewCommand);
	UFUNCTION(BlueprintNativeEvent)
	void SetImageToCommandBP(int NewCommand);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void StartPlay_Multicast();

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void EndGameWin_OnServer(int ScoreOne, int ScoreTwo, FName NameTeamWin, UObject* UiIconTeamWin);
	UFUNCTION(NetMulticast, Reliable)
	void EndGameWin_Multicast(int ScoreOne, int ScoreTwo, FName NameTeamWin, UObject* UiIconTeamWin);
	UFUNCTION(BlueprintNativeEvent)
	void EndGameWin_BP(int ScoreOne, int ScoreTwo, FName NameTeamWin, UObject* UiIconTeamWin);

	//UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	//TArray<AOKBaseCharacter*> BasePlayers;
	//UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "NetInformation")
	//AOneKillGameModeBase* BaseGameMode;
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetCurrentAmmoWeapon_Multicast(int Ammo, AOKBaseWeapon* Weapon);
	UFUNCTION(BlueprintNativeEvent)
	void SetCurrentAmmoWeapon_Multicast_BP(int Ammo, AOKBaseWeapon* Weapon);

	FAdditionalWeaponInfo AditionalInfo;
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetScoreToRespawn_OnServer(int ScoreOne, int ScoreTwo);
	UFUNCTION(NetMulticast, Reliable)
	void SetScoreToRespawn_Multicast(int ScoreOne, int ScoreTwo);
	UFUNCTION(BlueprintNativeEvent)
	void SetScoreToRespawn_BP(int ScoreOne, int ScoreTwo);


	UFUNCTION(NetMulticast, Reliable)
	void SetScoreToCommandSpectator_Multicast(int NewScore, int Id);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetScoreToCommandSpectator_OnServer(int NewScore, int Id);
	UFUNCTION(BlueprintNativeEvent)
	void SetScoreToCommandOneSpectatorBP(int NewScore);
	UFUNCTION(BlueprintNativeEvent)
	void SetScoreToCommandTwoSpectatorBP(int NewScored);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetIdPlayerStalker_Multicast(int NewId);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetNameToSpectator_Multicast(FName Name);
	UFUNCTION(BlueprintNativeEvent)
	void SetNameToSpectatorBP(FName Name);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetArmorToSpectator_Multicast(AOKBaseCharacter* PlayerStalker);
	UFUNCTION(BlueprintNativeEvent)
	void SetArmorToSpectatorBP(AOKBaseCharacter* PlayerStalker);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetHealthToSpectator_Multicast(AOKBaseCharacter* PlayerStalker);
	UFUNCTION(BlueprintNativeEvent)
	void SetHealthToSpectatorBP(AOKBaseCharacter* PlayerStalker);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetScoreToCommandSpectatorStart_Multicast(int ScoreComandOne, int ScoreComandTwo);

	UFUNCTION(BlueprintNativeEvent)
	void SetScoreToCommandSpectatorStartBP(int ScoreComandOne, int ScoreComandTwo);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void SetCurrentAmmoBySpectator_Multicast(AOKBaseWeapon* Weapon, int Id);
	UFUNCTION(BlueprintNativeEvent)
	void SetCurrentAmmoBySpectatorBP(AOKBaseWeapon* Weapon, int Id);


	

	

	


	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = "Character | NetWork")
	int CurrentIdStalker;

protected:
	virtual void SetupInputComponent() override;
	

private:

	void MoveForward(float Value);
	void MoveRight(float Value);
	void Turn(float Value);
	void LookUp(float Value);
	void Jump();
	void Aiming();
	void DeaAiming();
	void ChangeCrouchState();
	void StartSprint();
	void StopSprint();
	void StartAttack();
	void StopAttack();
	void TryReload();
	void SwitchNextWeapon();
	void SwitchPreviosWeapon();
	void ActionWeaponSlot1();
	void ActionWeaponSlot2();
	void ScoreBoard();
	
protected:
	void SwitchPlayerSpectatorNext();
	void SwitchPlayerSpectatorPrivious();
	UFUNCTION(BlueprintCallable)
	int GetCurrentPlayerId(AOKBaseCharacter* PlayerStalker);
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SwitchPlayerSpectatorNext_BP();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SwitchPlayerSpectatorPrivious_BP();

	UPROPERTY(Replicated, BlueprintReadOnly, EditAnywhere, Category = "Character | Network")
	TSoftObjectPtr<AOKBaseCharacter> CachedBaseCharacter;
	TSoftObjectPtr<class AOneKillGameModeBase> CachedBaseGameMode;
	
};
