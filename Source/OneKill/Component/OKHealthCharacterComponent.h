// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OKHealthComponent.h"
#include "OKHealthCharacterComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDestroyShield);
UCLASS()
class ONEKILL_API UOKHealthCharacterComponent : public UOKHealthComponent
{
	GENERATED_BODY()
	

public:
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnShieldChange ShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
	FOnDestroyShield DestroyShield;
	virtual void IncominDamage(float ChangeValue) override;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Replicated, Category = "Health")
	float Shield = 100.0f;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	UFUNCTION(BlueprintCallable, Category = "Health")
	void SetCurrentShield(float CurrentShield);
};
