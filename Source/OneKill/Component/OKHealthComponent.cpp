// Fill out your copyright notice in the Description page of Project Settings.


#include "OKHealthComponent.h"
#include "../Character/OKBaseCharacter.h"

// Sets default values for this component's properties
UOKHealthComponent::UOKHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOKHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	OnHeaithChange.Broadcast(Health, 0.0f);
	// ...
	
}


// Called every frame
void UOKHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UOKHealthComponent::GetCurreHealth()
{
	return Health;
}

void UOKHealthComponent::SetCurrentHealth(float NewHealth)
{
	OnHeaithChange.Broadcast(NewHealth, 0.0f);
	Health = NewHealth;
}

void UOKHealthComponent::IncominDamage(float ChangeValue)
{
	Health += ChangeValue;
	OnHeaithChange.Broadcast(Health, ChangeValue);
	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	if (Health <= 0.0f)
	{
		Health = 0.0f;
		OnDead.Broadcast();
		AOKBaseCharacter* myChar = Cast<AOKBaseCharacter>(GetOwner());
		myChar->CharDead();
	}
}

void UOKHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UOKHealthComponent, Health);

}

