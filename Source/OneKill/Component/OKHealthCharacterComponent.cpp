// Fill out your copyright notice in the Description page of Project Settings.


#include "OKHealthCharacterComponent.h"
#include "../Character/OKBaseCharacter.h"

void UOKHealthCharacterComponent::BeginPlay()
{
	Super::BeginPlay();
	ShieldChange.Broadcast(Shield, 0.0f);
}

void UOKHealthCharacterComponent::IncominDamage(float ChangeValue)
{
	if (Shield <= 0.0f)
	{
		Health += ChangeValue;
		OnHeaithChange.Broadcast(Health, ChangeValue);
		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		if (Health <= 0.0f)
		{
			Health = 0.0f;
			if (Health<=0.0f)
			{
				Health = 0.0f;
			}
			OnDead.Broadcast();
			AOKBaseCharacter* myChar = Cast<AOKBaseCharacter>(GetOwner());
			myChar->CharDead();
		}

	}
	else
	{
		
		Shield += ChangeValue;
		ShieldChange.Broadcast(Shield, ChangeValue);
		//Health = 100.0f;
	}

}

void UOKHealthCharacterComponent::SetCurrentShield(float CurrentShield)
{
	ShieldChange.Broadcast(Shield, 0.0f);
	Shield = CurrentShield;
}
void UOKHealthCharacterComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UOKHealthCharacterComponent, Shield);
}


