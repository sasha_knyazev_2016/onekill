// Fill out your copyright notice in the Description page of Project Settings.


#include "OKBaseWeapon.h"
#include "../Game/OKBaseGameInstance.h"
#include "../Character/OKBaseCharacter.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/AnimationAsset.h"
#include "../Component/OKInventoryComponent.h"

// Sets default values
AOKBaseWeapon::AOKBaseWeapon()
{
	SetReplicates(true);
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;


	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshComponent"));
	SkeletalMeshComponent->SetGenerateOverlapEvents(false);
	SkeletalMeshComponent->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshComponent->SetupAttachment(RootComponent);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetGenerateOverlapEvents(false);
	StaticMeshComponent->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshComponent->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AOKBaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

void AOKBaseWeapon::SetSkeletalMesh_Multicast_Implementation()
{
	SkeletalMeshComponent->SetSkeletalMesh(InSkelMesh);
}



// Called every frame
void AOKBaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (HasAuthority())
	{
		FireTick(DeltaTime);
		ReloadTick(DeltaTime);
		DispertionTick(DeltaTime);
		ClipDropTick(DeltaTime);
		ShellDropTick(DeltaTime);
	}
}

bool AOKBaseWeapon::FireTick(float DeltaTime)
{

	if (WeaponFiring && GetWeaponRound()> 0 && !WeaponRealoading)
	{
		if (FireTimer < 0.0f)
		{
			Fire();
			return true;
		}
			else
				FireTimer -= DeltaTime;
	}
	return false;
}

void AOKBaseWeapon::ReloadTick(float DeltaTime)
{
	if (WeaponRealoading)
	{
		if (ReloadTimer<0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AOKBaseWeapon::DispertionTick(float DeltaTime)
{
	if (!WeaponRealoading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}
		if (CurrentDispersion<CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion >CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
}

void AOKBaseWeapon::CancelReload()
{
	WeaponRealoading = false;
	if (SkeletalMeshComponent && SkeletalMeshComponent->GetAnimInstance())
	{
		SkeletalMeshComponent->GetAnimInstance()->StopAllMontages(0.15f);
	}
	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;
}

bool AOKBaseWeapon::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UOKInventoryComponent* myInventory = Cast<UOKInventoryComponent>(GetOwner()->GetComponentByClass(UOKInventoryComponent::StaticClass()));
		if (myInventory)
		{
			int8 AviableAmmoWeapon;
			if (!myInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoWeapon))
			{

				result = false;
			}
		}
	}
	return result;
}

int8 AOKBaseWeapon::GetAviableAmmoForReload()
{
	int8 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UOKInventoryComponent* myInventory = Cast<UOKInventoryComponent>(GetOwner()->GetComponentByClass(UOKInventoryComponent::StaticClass()));
		if (myInventory)
		{
			
			if (myInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				AviableAmmoForWeapon = AviableAmmoForWeapon;
			}
		}
	}
	return AviableAmmoForWeapon;
}

void AOKBaseWeapon::WeaponInit(FWeaponInfo InfoWeapon)
{
	if (SkeletalMeshComponent && !SkeletalMeshComponent->SkeletalMesh)
	{
		SkeletalMeshComponent->DestroyComponent();
	}
	if (StaticMeshComponent && !StaticMeshComponent->GetStaticMesh())
	{
		StaticMeshComponent->DestroyComponent();
	}
}

void AOKBaseWeapon::ClipDropTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0.0f)
		{
			DropClipFlag = false;
			InitDropMesh(WeaponSetting.ClipDropMesh.DropMesh, WeaponSetting.ClipDropMesh.DropMeshOffset, WeaponSetting.ClipDropMesh.DropMeshImpulseDir, WeaponSetting.ClipDropMesh.DropMeshLifeTime, WeaponSetting.ClipDropMesh.ImpulseRandomDispersion, WeaponSetting.ClipDropMesh.PowerImpulse, WeaponSetting.ClipDropMesh.CustomMass);
		}
		else
			DropClipTimer -= DeltaTime;
	}
}
void AOKBaseWeapon::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset, WeaponSetting.ShellBullets.DropMeshImpulseDir, WeaponSetting.ShellBullets.DropMeshLifeTime, WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpulse, WeaponSetting.ShellBullets.CustomMass);
		}
		else
			DropShellTimer -= DeltaTime;
	}
}
void AOKBaseWeapon::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
	}
}



bool AOKBaseWeapon::CheckWeaponCanFire()
{
	return true;
}

FProjectileInfo AOKBaseWeapon::GetProjectile()
{
	return WeaponSetting.ProjectileSettings;;
}

void AOKBaseWeapon::AnimFire_Multicast_Implementation()
{
	
}

void AOKBaseWeapon::Fire()
{
	//
	FOnWeaponFire.Broadcast(WeaponSetting.AnimaCharFire, WeaponSetting.FPSAnimaCharFireWeapon, WeaponSetting.AnimFireWeaponFPSMesh, WeaponSetting.AnimFireWeaponMesh, WeaponSetting.AnimaCharFireAim);

	UAnimMontage* AnimToPlay = nullptr;
	if (WeaponAiming)
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponFire;
	else
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponFire;

	if (WeaponSetting.AnimWeaponInfo.AnimWeaponFire
		&& SkeletalMeshComponent
		&& SkeletalMeshComponent->GetAnimInstance())
	{
		AnimWeapon_Multicast(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
		//SkeletalMeshComponent->GetAnimInstance()->Montage_Play(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
	}

	if (WeaponSetting.ShellBullets.DropMesh)
	{
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
		{
			InitDropMesh(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset, WeaponSetting.ShellBullets.DropMeshImpulseDir, WeaponSetting.ShellBullets.DropMeshLifeTime, WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpulse, WeaponSetting.ShellBullets.CustomMass);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}

	
	FireTimer = WeaponSetting.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());
	if (ShootLocation)
	{
		//myOwner->Shoot();
		/*
		* FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();
		
		if (ProjectileInfo.Projectile)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();
			
			AOKBaseProjectile* myProjectile = Cast<AOKBaseProjectile>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
		
			if (myProjectile)
			{
				myProjectile->InitialLifeSpan = 20.0f;
				SpawnRotation = FRotator(SpawnRotation.Roll, SpawnRotation.Pitch + 1.0, SpawnRotation.Yaw);
			}

			myProjectile->myWeaponOwner = this;
			CurrentProjectile = myProjectile;
			SetKillerByRrojectile();
		}
		*/
		
	}

	if(GetWeaponRound()<=0 && !WeaponRealoading)
	{
		if (CheckCanWeaponReload())
		{
			IniteReload();
		}
	}
}


int32 AOKBaseWeapon::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AOKBaseWeapon::IniteReload()
{
	WeaponRealoading = true;
	ReloadTimer = WeaponSetting.ReloadTime;
	UAnimMontage* AnimToPlay = nullptr;
	if (WeaponAiming)
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReloadAim;
	else
		AnimToPlay = WeaponSetting.AnimWeaponInfo.AnimCharReload;
	UAnimMontage* AnimWeaponToPlay = nullptr;
	if (WeaponAiming)
		AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReloadAim;
	else
		AnimWeaponToPlay = WeaponSetting.AnimWeaponInfo.AnimWeaponReload;
	AnimWeapon_Multicast(AnimToPlay);
	//WeaponReloadStartEvent_Multicast(AnimToPlay);
	OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload,WeaponSetting.FPSAnimReload, WeaponSetting.FPSAnimReloadWeapon, WeaponSetting.AnimReloadWeapon);

	if (WeaponSetting.AnimCharReload)
	{
		AnimWeapon_Multicast(WeaponSetting.AnimCharReload);
		//WeaponReloadStartEvent_Multicast(WeaponSetting.AnimCharReload);
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload,WeaponSetting.FPSAnimReload, WeaponSetting.FPSAnimReloadWeapon, WeaponSetting.AnimReloadWeapon);
	}
	if (WeaponSetting.AnimWeaponInfo.AnimWeaponReload
		&& SkeletalMeshComponent
		&& SkeletalMeshComponent->GetAnimInstance())//Bad Code? maybe best way init local variable or in func
	{
		SkeletalMeshComponent->GetAnimInstance()->Montage_Play(AnimWeaponToPlay);
	}
	if (WeaponSetting.ClipDropMesh.DropMesh)
	{
		DropClipFlag = true;
		DropClipTimer = WeaponSetting.ClipDropMesh.DropMeshTime;
	}

}

void AOKBaseWeapon::FinishReload()
{
	WeaponRealoading = false;
	int8 AviableAmmoFromInventory =  GetAviableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;
	if (NeedToReload> AviableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round = AviableAmmoFromInventory;
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AOKBaseWeapon::UpdateDispertionWeapon(float DispersionMaxUpdate, float DispertionMinUpdate, float DisprtionRecoilUpdate, float DispertionReductionUpdate)
{
	CurrentDispersionMax = DispersionMaxUpdate;
	CurrentDispersionMin = DispertionMinUpdate;
	CurrentDispersionRecoil = DisprtionRecoilUpdate;
	CurrentDispersionReduction = DispertionReductionUpdate;
}

FVector AOKBaseWeapon::GetFireEndLocation() const
{
	return FVector();
}

int8 AOKBaseWeapon::GetNumberProjectileByShot() const
{
	return 1; //WeaponSetting.NumberProjectileByShot;
}

void AOKBaseWeapon::SetKillerByRrojectile_Implementation()
{
}

void AOKBaseWeapon::InitDropMesh_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (DropMesh)
	{
		FTransform Transform;

		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X + this->GetActorRightVector() * Offset.GetLocation().Y + this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());

		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());
		ShellDropFier_Multicast(DropMesh, Transform, DropImpulseDirection, LifeTimeMesh, ImpulseRandomDispersion, PowerImpulse, CustomMass, LocalDir);
	}
}

void AOKBaseWeapon::FireAnimPlay_BP_Implementation()
{

}

void AOKBaseWeapon::WeaponReloadStartEvent_Multicast_Implementation(UAnimMontage* Animreload)
{
	OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload,WeaponSetting.FPSAnimReload, WeaponSetting.FPSAnimReloadWeapon, WeaponSetting.AnimReloadWeapon);
}

void AOKBaseWeapon::AnimWeapon_Multicast_Implementation(UAnimMontage* AnimFier)
{
	if (AnimFier && SkeletalMeshComponent && SkeletalMeshComponent->GetAnimInstance())
	{
		SkeletalMeshComponent->GetAnimInstance()->Montage_Play(AnimFier);
	}
}

void AOKBaseWeapon::ShellDropFier_Multicast_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir)
{
	FActorSpawnParameters Param;
	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;
	AStaticMeshActor* NewActor = nullptr;
	NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, Param);



	if (NewActor && NewActor->GetStaticMeshComponent())
	{
		NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

		//set parameter for new actor
		NewActor->SetActorTickEnabled(false);
		NewActor->InitialLifeSpan = LifeTimeMesh;

		NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		NewActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);
		//NewActor->GetStaticMeshComponent()->SetCollisionObjectType()



		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);



		if (CustomMass > 0.0f)
		{
			NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
		}

		if (!DropImpulseDirection.IsNearlyZero())
		{
			FVector FinalDir;
			LocalDir = LocalDir + (DropImpulseDirection * 1000.0f);

			if (!FMath::IsNearlyZero(ImpulseRandomDispersion))
				FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
			FinalDir.GetSafeNormal(0.0001f);

			NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
		}
	}
}

void AOKBaseWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AOKBaseWeapon, AdditionalWeaponInfo);
	DOREPLIFETIME(AOKBaseWeapon, FOnWeaponFire);
	DOREPLIFETIME(AOKBaseWeapon, WeaponAiming);
	DOREPLIFETIME(AOKBaseWeapon, WeaponFiring);
	DOREPLIFETIME(AOKBaseWeapon, CurrentSkeletalMesh);
	//DOREPLIFETIME(AOKBaseWeapon, CurrentSkelMesh);
	DOREPLIFETIME(AOKBaseWeapon, InSkelMesh);
	
	
}