// Fill out your copyright notice in the Description page of Project Settings.


#include "OKBaseProjectile.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AOKBaseProjectile::AOKBaseProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);
	BulletSpherComponent = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	BulletSpherComponent->SetSphereRadius(16.0f);

	

	BulletSpherComponent->bReturnMaterialOnMove = true;

	BulletSpherComponent->SetCanEverAffectNavigation(false);

	RootComponent = BulletSpherComponent;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BulletFX"));
	BulletFX->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject <UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.0f;
	BulletProjectileMovement->MaxSpeed = 0.0f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;

}

// Called when the game starts or when spawned
void AOKBaseProjectile::BeginPlay()
{
	Super::BeginPlay();
	BulletSpherComponent->OnComponentHit.AddDynamic(this, &AOKBaseProjectile::BulletCollisionSphereHit);
	BulletSpherComponent->OnComponentBeginOverlap.AddDynamic(this, &AOKBaseProjectile::BulletCollisionSpherBeginOverlap);
	BulletSpherComponent->OnComponentEndOverlap.AddDynamic(this, &AOKBaseProjectile::BulletCollisionSpherEndOverlap);
}

// Called every frame
void AOKBaseProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AOKBaseProjectile::InitProjectile(FProjectileInfo InitInfo)
{
	BulletProjectileMovement->InitialSpeed = InitInfo.ProjectileInitSpeed;
	this->SetLifeSpan(InitInfo.ProjectileLifeTime);
	if (InitInfo.ProjectileStaticMesh)
	{
		InitVisualMeshProjectile_Multicast(InitInfo.ProjectileStaticMesh, InitInfo.ProjectileStaticMeshOffset);
	}
	else
		BulletMesh->DestroyComponent();

	if (InitInfo.ProjectileTrailFx)
	{
		InitVisualTrailProjectile_Multicast(InitInfo.ProjectileTrailFx, InitInfo.ProjectileTrailFxOffset);
	}
	else
		BulletFX->DestroyComponent();

	ProjectileSetting = InitInfo;
	
}

void AOKBaseProjectile::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
		if (ProjectileSetting.HitDecals.Contains(mySurfacetype))
		{
			UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[mySurfacetype];
			if (myMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.Normal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
			}
		}
		if (ProjectileSetting.HitFXs.Contains(mySurfacetype))
		{
			UParticleSystem* myParticle = ProjectileSetting.HitFXs[mySurfacetype];
			if (myParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}
		if (ProjectileSetting.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(),ProjectileSetting.HitSound, Hit.ImpactPoint);
		}
	}
	//UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.ProjectileDamage, GetInstigatorController(),this, NULL);
	//UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
	
	ImpactProjectile();
}

void AOKBaseProjectile::BulletCollisionSpherBeginOverlap(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AOKBaseProjectile::BulletCollisionSpherEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AOKBaseProjectile::ImpactProjectile()
{
	this->Destroy();
}



void AOKBaseProjectile::InitVisualTrailProjectile_Multicast_Implementation(UParticleSystem* NewTemplate, FTransform MeshRelative)
{
	BulletFX->SetTemplate(NewTemplate);
	BulletFX->SetRelativeTransform(MeshRelative);
}
void AOKBaseProjectile::InitVisualMeshProjectile_Multicast_Implementation(UStaticMesh* newMesh, FTransform MeshRelative)
{
	BulletMesh->SetStaticMesh(newMesh);
	BulletMesh->SetRelativeTransform(MeshRelative);
}



