// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OKBaseProjectile.h"
#include "Net/UnrealNetwork.h"
#include "Engine/DataTable.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/Actor.h"
#include "OKBaseWeapon.generated.h"



UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	ShotGunType UMETA(DisplayName = "ShotGun"),
	SniperRifle UMETA(DisplayName = "SniperRifle"),
	GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
	RocketLauncher UMETA(DisplayName = "RocketLauncher")
};
USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Aim_StateDispersionReduction = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Walk_StateDispersionReduction = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
	float Run_StateDispersionReduction = 0.1f;


};
USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharReloadAim = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
	UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
	UAnimMontage* AnimWeaponReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
	UAnimMontage* AnimWeaponFire = nullptr;
};
USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	UStaticMesh* DropMesh = nullptr;
	//0.0f immediately drop
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
	float DropMeshTime = -1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
	float DropMeshLifeTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
	FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
	FVector DropMeshImpulseDir = FVector(0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
	float PowerImpulse = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
	float ImpulseRandomDispersion = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
	float CustomMass = 0.0f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
	TSubclassOf<class AOKBaseWeapon> WeaponClass = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 MaxRound = 10;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	//int8 NumberProjectileByShot = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	FWeaponDispersion DispertionWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	UParticleSystem* EffectFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	FProjectileInfo ProjectileSettings;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponInfo")
	float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* AnimCharReload = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	FProjectileInfo ProjectileSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	FAnimationWeaponInfo AnimWeaponInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
	FDropMeshInfo ClipDropMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
	FDropMeshInfo ShellBullets;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace ")
	float DistacneTrace = 2000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* AnimaCharFire;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* AnimaCharFireAim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* AnimaCharSwitchWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* FPSAnimaCharFireWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* FPSAnimaCharSwitchWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* FPSAnimReload;


	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* AnimFireWeaponFPSMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* AnimFireWeaponMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* FPSAnimReloadWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AnimChar")
	UAnimMontage* AnimReloadWeapon;


	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	float SwitchTimeToWeapon = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory ")
	EWeaponType WeaponType = EWeaponType::RifleType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Random")
	float RandomYMin = -0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Random")
	float RandomYMax = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Random")
	float RandomZMin = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Random")
	float RandomZMax = 0.04f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Random")
	bool bIsSnip = false;
	
};



USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSettings")
	int32 Round = 10;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FName NameItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	///Index Slot by Index Array
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 Cout = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 MaxCout = 100;
};
USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

	///Index Slot by Index Array
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	USkeletalMesh* WeaponSkeletMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	UParticleSystem* ParticleItem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	FTransform Ofset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	FWeaponSlot WeaponInfo;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnWeaponReloadStart, UAnimMontage*, AnimReload, UAnimMontage*, FPSAnimReload,  UAnimMontage*, FPSAnimReloadWeapon, UAnimMontage*, AnimReloadWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FiveParams (FOnWeaponFire, UAnimMontage*, AnimFireChar, UAnimMontage*, AnimFireFPSChar, UAnimMontage*, AnimFireWeaponFPS, UAnimMontage*, AnimFireWeapon, UAnimMontage*, AnimFireAnim);

UCLASS()
class ONEKILL_API AOKBaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOKBaseWeapon();

	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	UPROPERTY(Replicated)
	FOnWeaponFire FOnWeaponFire;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	class USkeletalMeshComponent* SkeletalMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	class USkeletalMeshComponent* CurrentSkeletalMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	class UStaticMeshComponent* StaticMeshComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Component")
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(BlueprintReadWrite)
	FWeaponInfo WeaponSetting;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo AdditionalWeaponInfo;
	
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	USkeletalMesh* InSkelMesh;


	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	USkeletalMesh* CurrentSkeletalMesh;
	
	
	
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void SetSkeletalMesh_Multicast();
public:


	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	bool FireTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
	void ReloadTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
	void DispertionTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
	void ClipDropTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
	void ShellDropTick(float DeltaTime);

	void CancelReload();
	bool CheckCanWeaponReload();
	int8 GetAviableAmmoForReload();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Network")
	class AOKBaseCharacter* myOwner;
	void WeaponInit(FWeaponInfo InfoWeapon);


	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponRealoading = false;
	UFUNCTION(Server,Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile();
	
	
	UFUNCTION(NetMulticast,Unreliable)
	void AnimFire_Multicast();

	UFUNCTION(BlueprintCallable)
	void Fire();
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	float FireTimer = 0.0f;
	float ReloadTimer = 0.0f;
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponAiming = false;

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();
	void IniteReload();
	void FinishReload();

	void UpdateDispertionWeapon(float DispersionMax, float DispertionMin, float DisprtionRecoil, float DispertionReduction);


	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;

	bool BlockFire = false;

	void IniteDropMesh();
	//Dispersion
	UPROPERTY()
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;
	UFUNCTION(BlueprintNativeEvent)
	void FireAnimPlay_BP();
	UFUNCTION(BlueprintNativeEvent)
	void SetKillerByRrojectile();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	AOKBaseProjectile* CurrentProjectile;
	UFUNCTION(Server, Reliable)
	void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = true;

	bool DropClipFlag = false;
	float DropClipTimer = -1.0;

	//shell flag
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;
	UFUNCTION(NetMulticast,Reliable)
	void WeaponReloadStartEvent_Multicast(UAnimMontage* Animreload);
	UFUNCTION(NetMulticast, Unreliable)
	void AnimWeapon_Multicast(UAnimMontage* AnimFier);
	UFUNCTION(NetMulticast, Unreliable)
	void ShellDropFier_Multicast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
private:
	
	TSoftObjectPtr<class AOKBaseCharacter> CachedBaseCharacter;
};
