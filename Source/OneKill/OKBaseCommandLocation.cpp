#include "OKBaseCommandLocation.h"
// Fill out your copyright notice in the Description page of Project Settings.
#include "Components/BoxComponent.h"

#include "OKBaseCommandLocation.h"

// Sets default values
AOKBaseCommandLocation::AOKBaseCommandLocation()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(FName("TriggerVolume"));
	TriggerVolume->OnComponentBeginOverlap.AddDynamic(this, &AOKBaseCommandLocation::OnCommandLocationBeginOverlap);
	TriggerVolume->OnComponentEndOverlap.AddDynamic(this, &AOKBaseCommandLocation::OnCommandLocationEndOverlap);
}

void AOKBaseCommandLocation::OnCommandLocationEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//CashBaseCharacter = Cast<AOKBaseCharacter>(OtherActor);
	//CashBaseCharacter.Get()->bIsCommandTarget = false;
}

void AOKBaseCommandLocation::OnCommandLocationBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//CashBaseCharacter = Cast<AOKBaseCharacter>(OtherActor);
	//CashBaseCharacter.Get()->bIsCommandTarget = true;
}

// Called when the game starts or when spawned
void AOKBaseCommandLocation::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AOKBaseCommandLocation::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

